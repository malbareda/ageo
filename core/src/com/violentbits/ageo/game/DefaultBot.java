package com.violentbits.ageo.game;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateTo;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class DefaultBot extends Bot {

	public DefaultBot(String textureName, Color flagColor) {
		super(textureName, flagColor);
	}

	@Override
	public void reset(WorldController worldController) {
		this.clearActions();
		super.reset(worldController);
		addAction(parallel(moveTo(initialX, initialY, worldController.actionTime*2),
				rotateTo(initialRotation, worldController.actionTime*2)));
	}
	
	@Override
	public void setActive(boolean active) {
		if (isActive() != active) {
			if (active)
				addAction(Actions.color(getActivatedColor(), worldController.actionTime));
			else
				addAction(Actions.color(getDeactivatedColor(), worldController.actionTime));
		}
		super.setActive(active);
	}
}
