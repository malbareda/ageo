package com.violentbits.ageo.game;

import com.violentbits.ageo.AgeoGame;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.io.LevelInfo;
import com.violentbits.ageo.io.Profile;

/**
 * WorldController used during normal game play. It takes care of saving data
 * to the profile's file whenever the level is tried or is beaten.
 */
public class DefaultWorldController extends WorldController {
	@SuppressWarnings("unused")
	private static final String TAG = DefaultWorldController.class.getName();
	
	public DefaultWorldController(AgeoGame game) {
		super(game);
	}

	@Override
	protected void reset() {
		super.reset();
		GamePreferences.instance.getCurrentProfile().levelTried();
	}
	
	@Override
	protected void win() {
		int nInstructions=0;
		int maxInst=0;
		for (Bot bot : getBots()) {
			if (bot.editable) {
				nInstructions+=bot.getProgram().getNInstructions();
				maxInst+=bot.getProgram().getCells().size;
			}
		}
		LevelInfo levelInfo = Assets.instance.getLevelInfo();
		Profile profile = GamePreferences.instance.getCurrentProfile();
		profile.levelWon(levelInfo.minSteps, levelInfo.minIns, maxInst, getNSteps(), nInstructions);
		super.win();
	}
}
