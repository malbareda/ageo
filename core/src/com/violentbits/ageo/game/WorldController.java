package com.violentbits.ageo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.violentbits.ageo.AgeoGame;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.LevelScreen;
import com.violentbits.ageo.game.WorldListener.EventType;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.victory.VictoryCondition;
import com.violentbits.ageo.victory.VictoryConditionFactory;

/**
 * The WorldController manages all game logic. This is a base class
 * that takes care of activating bots, check victory condition, running
 * and pausing the game, etc.
 */
public abstract class WorldController extends InputAdapter {
	@SuppressWarnings("unused")
	private static final String TAG = WorldController.class.getName();
	/**
	 * An array containing all bots in the current level.
	 */
	private Array<Bot> bots = new Array<Bot>();
	/**
	 * Index in bots of the currently active bot.
	 */
	private int currentBotIndex;
	/**
	 * Number of instructions that the current bot will execute before
	 * the next one is activated.
	 */
	private int currentBotSpeed;
	/**
	 * Accumulated delta time since the last bot action was executed.
	 */
	private float accDelta;
	/**
	 * A reference to the AgeoGame.
	 */
	private AgeoGame game;
	/**
	 * Victory condition for the current level.
	 */
	private VictoryCondition victoryCondition;
	/**
	 * A reference to the map controller.
	 */
	private MapController mapController = new MapController();
	/**
	 * true if the user has pressed the play button.
	 */
	private boolean running;
	/**
	 * true if the user has pressed the pause button.
	 */
	private boolean paused;
	/**
	 * true if the current level has just been beaten.
	 */
	private boolean won;
	/**
	 * The time between two calls to the bot's action method, in seconds. 
	 */
	public float actionTime = Constants.ACTION_TIME_SLOW;
	/**
	 * A list of listeners that will be notified of changes.
	 */
	private Array<WorldListener> listeners = new Array<WorldListener>();
	/**
	 * Number of steps that all editable bots have executed since the last restart.
	 */
	private int nSteps;
	/**
	 * Constructor.
	 * 
	 * @param game
	 */
	public WorldController(AgeoGame game) {
		this.game = game;
		init();
	}
	/**
	 * Adds a listener that will be notified of changes.
	 * 
	 * @param worldListener
	 */
	public void addListener(WorldListener worldListener) {
		listeners.add(worldListener);
	}
	/**
	 * Notify all listener of an event concerning this world.
	 * 
	 * @param event
	 * @param object  Optional object including more information about the event.
	 */
	public void notifyEvent(WorldListener.EventType event, Object object) {
		for (WorldListener listener : listeners) {
			listener.worldChanged(event, object);
		}
	}
	/**
	 * Returns a list of all bots in this world.
	 * 
	 * @return
	 */
	public Array<Bot> getBots() {
		return bots;
	}
	/**
	 * Returns a references to the map controller associated with the current map level.
	 * 
	 * @return
	 */
	public MapController getMapController() {
		return mapController;
	}
	/**
	 * Initializes the world controller.
	 */
	public void init() {
		won=false;
		// Remove bots from stage
		for (Bot bot : bots) {
			bot.remove();
		}
		bots.clear();
		accDelta = 0;
		bots = Assets.instance.getBots();
		mapController.setMap(Assets.instance.getMap());
		victoryCondition = VictoryConditionFactory.getVictoryCondition(this);
		notifyEvent(WorldListener.EventType.INIT, null);
		reset();
	}
	/**
	 * Executes next bot instruction.
	 */
	protected void action() {
		Bot currentBot;
		int lastBotIndex;
		
		if (victoryCondition!=null && victoryCondition.checkVictory(this)) {
			win();
		} else {
			currentBot = bots.get(currentBotIndex);
			currentBot.action();
			notifyEvent(EventType.ACTION, currentBot);
			if (currentBot.editable)
				nSteps++;
			lastBotIndex = currentBotIndex;
			currentBotSpeed--;
			if (currentBotSpeed<=0) {
				do {
					currentBotIndex = (currentBotIndex+1)%bots.size;
				} while (currentBotIndex!=lastBotIndex && !bots.get(currentBotIndex).isActive());
				currentBotSpeed = bots.get(currentBotIndex).getSpeed();
			}
		}
	}
	/**
	 * Updates the world. Calls action() if necessary.
	 * 
	 * @param delta
	 */
	public void update(float delta) {
		boolean go = true;
		
		if (!paused && running && !won) {
			accDelta += delta;
			for (Bot bot : bots) {
				if (bot.hasActions())
					go = false;
				if (!Timer.instance().isEmpty())
					go = false;
			}
			if (go && accDelta > actionTime) {
				accDelta = accDelta % actionTime;
				action();
			}
		}
	}
	/**
	 * Starts all bot programs.
	 * 
	 * @param actionTime  The time between two actions.
	 */
	public void run(float actionTime) {
		this.actionTime = actionTime;
		accDelta = 0;
		paused=false;
		if (bots.size>0 && !running) {
			for (Bot bot : bots) {
				bot.getProgram().makeBackup();
				bot.getProgram().setCounter(0);
			}
			running = true;
			currentBotSpeed = bots.first().getSpeed();
		}
		notifyEvent(WorldListener.EventType.RUN, actionTime);
	}
	/**
	 * Stops all bots. Resets the world to its initial state.
	 */
	public void stop() {
		if (running) {
			notifyEvent(WorldListener.EventType.STOP, null);
			reset();
		}
	}
	/**
	 * Pauses the world. No new actions will be executed until
	 * run() is called.
	 */
	public void pause() {
		paused = true;
		notifyEvent(WorldListener.EventType.PAUSE, null);
	}
	/**
	 * Resets the world to its initial state.
	 */
	protected void reset() {
		Timer.instance().clear();
		paused = false;
		running = false;
		currentBotIndex=0;
		nSteps=0;
		for (Bot bot : bots)
    	   	bot.reset(this);
		notifyEvent(WorldListener.EventType.RESET, null);
	}
	/**
	 * Checks if bots programs are running.
	 * 
	 * @return true if bots programs are running, false otherwise.
	 */
	public boolean isRunning() {
		return running;
	}
	/**
	 * Returns true if the level has been beaten, false otherwise.
	 * 
	 * @return
	 */
	public boolean hasWon() {
		return won;
	}
	/**
	 * Considers the current level beaten.
	 */
	protected void win() {
		won=true;
		notifyEvent(WorldListener.EventType.WON, null);
	}
	/**
	 * Reacts to key events. Return to the level screen if escape or back is pressed.
	 */
	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {
			returnToLevelScreen();
		}
		return false;
	}
	/**
	 * Leaves this level and returns to the level selection screen.
	 */
	public void returnToLevelScreen() {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				game.setScreen(new LevelScreen(game, won));
			}
	    });
	}
	/**
	 * Returns the bot that occupies a position in the world, or null if there
	 * isn't any bot there.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public Bot getBotAtPosition(float x, float y) {
		Bot foundBot = null;
		for (Bot bot : bots) {
			if (MathUtils.isEqual(bot.getX(), x, 0.4f) &&
					MathUtils.isEqual(bot.getY(), y, 0.4f)) {
				foundBot = bot;
			}
		}
		return foundBot;
	}
	/**
	 * Call this when an actor is added to the world stage. This method
	 * only notifies listeners of the addition, it does nothing more.
	 * 
	 * @param actor
	 */
	public void addActor(Actor actor) {
		notifyEvent(WorldListener.EventType.ACTOR_ADDED, actor);
	}
	/**
	 * Returns a reference to the victory condition for the current level.
	 * 
	 * @return
	 */
	public VictoryCondition getVictoryCondition() {
		return victoryCondition;
	}
	/**
	 * Returns the index that the active bot has in the bots array.
	 * 
	 * @return
	 */
	public int getCurrentBotIndex() {
		return currentBotIndex;
	}
	/**
	 * Returns the number of instructions that the active bot will
	 * execute before another bot take its place.
	 * 
	 * @return
	 */
	public int getCurrentBotSpeed() {
		return currentBotSpeed;
	}
	/**
	 * Returns the total number of steps that editable bots have executed
	 * since the last time the world was reseted.
	 * 
	 * @return
	 */
	public int getNSteps() {
		return nSteps;
	}
}
