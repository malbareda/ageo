package com.violentbits.ageo.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.apparat.InstructionsTable;
import com.violentbits.ageo.apparat.ProgramTable;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.scene.BlinkingButton;
import com.violentbits.ageo.scene.SelectedActor;
import com.violentbits.ageo.scene.StatsWindow;
import com.violentbits.ageo.tutorial.Tutorial;

public class HudRenderer extends ScreenAdapter implements WorldListener {
	private static final String TAG = HudRenderer.class.getName();
	private WorldController worldController;
	private ExtendViewport hudViewport;
	private ExtendViewport worldViewport;
	private SpriteBatch batch;
	private BitmapFont font;

	private Stage hudStage;
	private Stage worldStage;
	private Table buttonTable;
	private BlinkingButton btnPlay;
	private Button btnStop;
	private BlinkingButton btnFastForward;
	private BlinkingButton btnPause;
	private BlinkingButton btnStep;
	private Button btnHelp;
	private Button btnClear;

	private OrthogonalTiledMapRenderer mapRenderer;
	
	private ProgramTable programTable;
	private InstructionsTable instructionsTable;
	private DragAndDrop dragAndDrop;
	
	private SelectedActor selectedActor = new SelectedActor();
	
	private ClickListener clickOnBot = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			SoundController.instance.play(Assets.instance.getSound("select.wav"));
			Bot bot = (Bot) event.getListenerActor();
			programTable.setBot(bot);
			instructionsTable.setBot(bot);
			instructionsTable.setVisible(bot.editable);
			selectedActor.setSelectedBot(bot);
		}
	};

	public HudRenderer(WorldController worldController) {
		this.worldController = worldController;
		worldController.addListener(this);
	}

	private void createStopButton() {
		btnStop = new Button(Assets.instance.createButtonStyle("stop"));
		btnStop.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (worldController.isRunning()) {
					SoundController.instance.play(Assets.instance.getSound("select.wav"));
					btnPlay.stopBlink();
					btnPause.stopBlink();
					btnFastForward.stopBlink();
					btnStep.stopBlink();
					worldController.stop();
				}
			}
		});
	}
	
	private void createPauseButton() {
		btnPause = new BlinkingButton(Assets.instance.createButtonStyle("pause"));
		btnPause.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (worldController.isRunning()) {
					SoundController.instance.play(Assets.instance.getSound("select.wav"));
					btnPause.startBlink();
					btnPlay.stopBlink();
					btnFastForward.stopBlink();
					btnStep.stopBlink();
					worldController.pause();
				}
			}
		});
	}
	
	private void createPlayButton() {
		btnPlay = new BlinkingButton(Assets.instance.createButtonStyle("play"));
		btnPlay.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				btnPlay.startBlink();
				btnPause.stopBlink();
				btnFastForward.stopBlink();
				btnStep.stopBlink();
				worldController.run(Constants.ACTION_TIME_SLOW);
			}
		});
	}
	
	private void createFastForwardButton() {
		btnFastForward = new BlinkingButton(Assets.instance.createButtonStyle("fastforward"));
		btnFastForward.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				btnFastForward.startBlink();
				btnPause.stopBlink();
				btnPlay.stopBlink();
				btnStep.stopBlink();
				worldController.run(Constants.ACTION_TIME_FAST);
			}
		});
	}
	
	private void createStepButton() {
		btnStep = new BlinkingButton(Assets.instance.createButtonStyle("step"));
		btnStep.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				btnFastForward.stopBlink();
				btnPause.stopBlink();
				btnPlay.stopBlink();
				btnStep.startBlink();
				worldController.run(Constants.ACTION_TIME_SLOW);
				btnPause.addAction(Actions.delay(Constants.ACTION_TIME_SLOW, Actions.run(new Runnable() {
					@Override
					public void run() {
						btnPause.fire(new ChangeEvent());
					}
				})));
			}
		});
	}
	
	private void createHelpButton() {
		btnHelp = new Button(Assets.instance.createButtonStyle("help"));
		btnHelp.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				if (worldController.getVictoryCondition() != null)
					worldController.getVictoryCondition().help(worldController, worldStage, hudStage);
			}
		});
	}
	
	private void createClearButton() {
		btnClear = new Button(Assets.instance.createButtonStyle("clear"));
		btnClear.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				programTable.clearCells();
			}
		});
	}
	
	private void createButtonTable() {
		buttonTable = new Table();
		buttonTable.left().top().padLeft(Constants.HUD_MARGIN).padTop(Constants.HUD_MARGIN);
		buttonTable.defaults().padRight(10).width(Constants.INSTRUCTION_ICON_SIZE).height(Constants.INSTRUCTION_ICON_SIZE);
		createStopButton();
		buttonTable.add(btnStop);
		createPauseButton();
		buttonTable.add(btnPause);
		createPlayButton();
		buttonTable.add(btnPlay);
		createFastForwardButton();
		buttonTable.add(btnFastForward);
		createStepButton();
		buttonTable.add(btnStep);
		createHelpButton();
		buttonTable.add(btnHelp);
		createClearButton();
		buttonTable.add(btnClear);
		btnStop.setChecked(true);
		buttonTable.setFillParent(true);
		hudStage.addActor(buttonTable);
	}
	
	private void createTutorial() {
		String[] tutorialInstructions = Assets.instance.getLevelInfo().tutorial;
		if (tutorialInstructions!=null) {
			new Tutorial(worldController, this, tutorialInstructions);
		}
	}
	
	private void buildStage() {
		hudStage.clear();
		worldStage.clear();
		createButtonTable();
		createMapRenderer();
		createInstructionTables();
		worldStage.addActor(selectedActor);
		createTutorial();
		resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
	
	private void createMapRenderer() {
		TiledMap map = Assets.instance.getMap();
		mapRenderer = new OrthogonalTiledMapRenderer(map, 1 / Constants.TILE_SIZE);
	}
	
	private void createInstructionTables() {
		dragAndDrop = new DragAndDrop();
		programTable = new ProgramTable(worldController, dragAndDrop);
		hudStage.addActor(programTable);
		instructionsTable = new InstructionsTable(worldController, dragAndDrop);
		hudStage.addActor(instructionsTable);

		Array<Bot> bots = worldController.getBots();
		for (Bot bot : bots) {
			worldStage.addActor(bot);
			bot.addListener(clickOnBot);
			if (bot.editable) {
				programTable.setBot(bot);
				instructionsTable.setBot(bot);
				selectedActor.setSelectedBot(bot);
			}
		}
	}

	@Override
	public void show() {
		Gdx.app.debug(TAG, "show called");
		hudViewport = new ExtendViewport(Constants.LEVEL_SCREEN_WIDTH, Constants.LEVEL_SCREEN_HEIGHT);
		worldViewport = new ExtendViewport(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT);
		batch = new SpriteBatch();
		font = new BitmapFont();
		font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		hudStage = new Stage(hudViewport);
		worldStage = new Stage(worldViewport);

		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(worldStage);
		inputMultiplexer.addProcessor(hudStage);
		inputMultiplexer.addProcessor(worldController);
		Gdx.input.setInputProcessor(inputMultiplexer);

		buildStage();
	}

	@Override
	public void resize(int width, int height) {
		hudViewport.update(width, height, true);
		font.getData().setScale(Math.min(width, height) / 640f);
		hudStage.getViewport().update(width, height, true);
		TiledMapTileLayer layer0 = (TiledMapTileLayer) Assets.instance.getMap().getLayers().get(0);
		worldStage.getCamera().position.set(layer0.getWidth() / 2f, layer0.getHeight() / 2f, 1);
		worldStage.getViewport().update(width, height, false);
		/*
		float s = Constants.INSTRUCTION_ICON_SIZE;
		float ww = hudViewport.getWorldWidth();
		float wh = hudViewport.getWorldHeight();
		Gdx.app.debug(TAG, "World size: "+ww+", "+wh);
		float sw = hudViewport.getScreenWidth();
		float sh = hudViewport.getScreenHeight();
		Gdx.app.debug(TAG, "Screen size: "+sw+", "+sh);
		float pcx = Gdx.graphics.getPpcX();
		float pcy = Gdx.graphics.getPpcY();
		Gdx.app.debug(TAG, "Pixel x cm: "+pcx+", "+pcy);
		float cx = s*sw/(ww*pcx);
		float cy = s*sh/(wh*pcy);
		Gdx.app.debug(TAG, "Instruction icon real size: "+cx+", "+cy);
		*/
	}

	@Override
	public void dispose() {
		Gdx.app.debug(TAG, "disposed called");
		batch.dispose();
		hudStage.dispose();
		worldStage.dispose();
		mapRenderer.dispose();
	}

	@Override
	public void render(float delta) {
		worldStage.act(delta);
		if (!worldController.isRunning() || !Constants.IS_SOLVER_ACTIVE) {
			hudStage.act(delta);
			Gdx.gl.glClearColor(Constants.DARK_BLUE.r, Constants.DARK_BLUE.g, Constants.DARK_BLUE.b, Constants.DARK_BLUE.a);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			mapRenderer.setView((OrthographicCamera) worldStage.getCamera());
			mapRenderer.render();
			worldStage.draw();
			if (Gdx.app.getLogLevel() == Application.LOG_DEBUG) {
				hudViewport.apply();
				batch.setProjectionMatrix(hudViewport.getCamera().combined);
				batch.begin();
				font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), Constants.HUD_MARGIN, Constants.HUD_MARGIN);
				batch.end();
			}
			hudStage.draw();
		} else {
			for (int i=0; i<10000; i++) {
				worldController.update(delta);
			}
			Gdx.graphics.requestRendering();
		}
	}
	
	public void setDisabled(boolean isDisabled) {
		for (Actor actor : buttonTable.getChildren()) {
			Button btn = (Button) actor;
			if (btn instanceof BlinkingButton) {
				((BlinkingButton) btn).stopBlink();
			}
			btn.setDisabled(isDisabled);
			btn.setTouchable(isDisabled?Touchable.disabled:Touchable.enabled);
		}
	}
	
	private void win() {
		SoundController.instance.stop();
		SoundController.instance.play(Assets.instance.getSound("win.ogg"));
		Image robowin = new Image(Assets.instance.get("robowin"));
		
		robowin.setOrigin(robowin.getWidth()/2f, robowin.getHeight()/2f);
		float scale = (hudStage.getWidth()/4f) / robowin.getWidth();
		robowin.setSize(robowin.getWidth()*scale, robowin.getHeight()*scale);
		robowin.setPosition(hudStage.getWidth()/4f-robowin.getWidth()/2f, -robowin.getHeight());
		hudStage.addActor(robowin);
		robowin.addAction(Actions.sequence(
			Actions.moveBy(0, Math.min(robowin.getHeight(), hudStage.getHeight()), 1.5f, Interpolation.bounceOut)
		));
		
		setDisabled(true);
		StatsWindow statsWindow = new StatsWindow(worldController);
		statsWindow.setPosition(robowin.getX()+robowin.getWidth()+20f, (hudStage.getHeight()-statsWindow.getHeight())/2f);
		hudStage.addActor(statsWindow);
	}

	@Override
	public void worldChanged(EventType event, Object object) {
		if (event == EventType.WON) {
			win();
		} else if (event == EventType.ACTOR_ADDED) {
			worldStage.addActor((Actor)object);
		} else if (event == EventType.RESET) {
			Camera cam = worldStage.getCamera();
			cam.up.set(0, 1, 0);
			cam.direction.set(0, 0, -1);
		}
	}

	public BlinkingButton getBtnPlay() {
		return btnPlay;
	}

	public Button getBtnStop() {
		return btnStop;
	}

	public BlinkingButton getBtnFastForward() {
		return btnFastForward;
	}

	public BlinkingButton getBtnPause() {
		return btnPause;
	}

	public BlinkingButton getBtnStep() {
		return btnStep;
	}

	public Button getBtnHelp() {
		return btnHelp;
	}
	
	public Button getBtnClear() {
		return btnClear;
	}

	public ProgramTable getProgramTable() {
		return programTable;
	}

	public InstructionsTable getInstructionsTable() {
		return instructionsTable;
	}
	
	public Stage getHudStage() {
		return hudStage;
	}
	
	public Stage getWorldStage() {
		return worldStage;
	}
}
