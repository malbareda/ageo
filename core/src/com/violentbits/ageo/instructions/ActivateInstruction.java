package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.Bots;
import com.violentbits.ageo.game.WorldController;

public class ActivateInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		boolean ok = false;
		if (!bot.isActive()) {
			bot.setActive(true);
			ok = true;
		} else {
			Bot facingBot = Bots.findFacingBot(worldController, bot);
			if (facingBot != null && !facingBot.isActive()) {
				facingBot.setActive(true);
				ok = true;
			}
		}
		return ok;
	}
}
