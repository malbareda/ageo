package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.MapController;
import com.violentbits.ageo.game.WorldController;

public class ChaseInstruction extends MovementInstruction {
	private static final String dir[] = {"Right","Left","Up","Down"};
	private static final int dirx[] = {1,-1,0,0};
	private static final int diry[] = {0,0,1,-1};

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		boolean ok = false;
		Bot target = null;
		int direction1=-1, direction2=-1, diraux;
		for (Bot b : worldController.getBots()) {
			if (b.editable != bot.editable && b.id != bot.id && b.isActive())
				target = b;
		}
		if (target!=null) {
			int dx = Math.round(target.getX()-bot.getX());
			int dy = Math.round(target.getY()-bot.getY());
			if (dx>0) direction1=0;
			else if (dx<0) direction1=1;
			if (dy>0) direction2=2;
			else if (dy<0) direction2=3;
			if (direction1==-1) {
				direction1=direction2;
				direction2=-1;
			} else if (direction2!=-1) {
				if (Math.abs(dy)>Math.abs(dx)) {
					diraux=direction2;
					direction2=direction1;
					direction1=diraux;
				}
			}
			ok=canMove(worldController, bot, target, dirx[direction1], diry[direction1], dir[direction1]);
			if (!ok && direction2!=-1) {
				direction1=direction2;
				ok=canMove(worldController, bot, target, dirx[direction1], diry[direction1], dir[direction1]);
			}
			if (ok) {
				ok=InstructionFactory.instance.getInstruction(dir[direction1]).run(worldController, bot);
			}
		}
		return ok;
	}
	
	private boolean canMove(WorldController worldController, Bot bot, Bot target, int dx, int dy, String direction) {
		boolean ok=true;
		MapController mapController = worldController.getMapController();
		if (mapController.getProperty("kill", bot.getX(), bot.getY()).contains(direction.toLowerCase())) {
			ok = false;
		} else if (mapController.getProperty("block", bot.getX(), bot.getY()).contains(direction.toLowerCase())) {
			ok = false;
		} else {
			// Check collisions between bots
			Bot otherBot = worldController.getBotAtPosition(bot.getX()+dx, bot.getY()+dy);
			if (otherBot != null && otherBot != bot && otherBot != target) {
				ok = false;
			}
		}
		return ok;
	}

}
