package com.violentbits.ageo.instructions;

import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public abstract class MoveToWallInstruction extends MovementInstruction {
	protected int dx;
	protected int dy;
	
	private void checkAdvance(Bot bot) {
		Program program = bot.getProgram();
		program.setCounter(program.getIndex(program.getCounter()-1));
	}
	
	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		boolean ok = runDefault(worldController, bot, dx, dy);
		if (ok) {
			checkAdvance(bot);
		}
		return ok;
	}
	
	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		boolean ok = runInstantly(worldController, bot, dx, dy);
		if (ok) {
			checkAdvance(bot);
		}
		return ok;
	}
}
