package com.violentbits.ageo.instructions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ObjectMap;

public class InstructionFactory {
	private static final String TAG = InstructionFactory.class.getName();
	public static final InstructionFactory instance = new InstructionFactory();

	private ObjectMap<String, Instruction> instructions = new ObjectMap<String, Instruction>();
	
	private InstructionFactory() {}
	
	public Instruction getInstruction(String name) {
		String[] nameParts = name.split("#");

		Instruction instruction = instructions.get(name);
		if (instruction == null) {
			String className = nameParts[0]+"Instruction";
			try {
				instruction = (Instruction) Class.forName(InstructionFactory.class.getPackage().getName()+"."+className).newInstance();
				instruction.setName(name);
				if (!(instruction instanceof StateInstruction))
					instructions.put(name, instruction);
				//Gdx.app.debug(TAG, "Instruction created: "+instruction.getName()+" "+instruction.toString());
			} catch (ClassNotFoundException e) {
				Gdx.app.log(TAG, e.toString());
			} catch (InstantiationException e) {
				Gdx.app.log(TAG, e.toString());
			} catch (IllegalAccessException e) {
				Gdx.app.log(TAG, e.toString());
			}
		}
		return instruction;
	}
	
	public Instruction getInstruction(Instruction instruction) {
		return getInstruction(instruction.getName());
	}
}
