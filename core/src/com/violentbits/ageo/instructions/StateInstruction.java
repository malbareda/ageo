package com.violentbits.ageo.instructions;

import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public abstract class StateInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		Program program = bot.getProgram();
		program.pushPendingInstruction(this);
		return true;
	}
	
	public abstract boolean end(WorldController worldController, Bot bot);
}
