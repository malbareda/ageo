package com.violentbits.ageo.instructions;

import com.badlogic.gdx.math.MathUtils;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class ForwardInstruction extends MovementInstruction {
	private Instruction getInstruction(Bot bot) {
		Instruction ins;
		float rotation = (720+bot.getRotation())%360;
		if (MathUtils.isEqual(rotation, 90f, 1f))
			ins = InstructionFactory.instance.getInstruction("Left");
		else if (MathUtils.isEqual(rotation, 180f, 1f))
			ins = InstructionFactory.instance.getInstruction("Down");
		else if (MathUtils.isEqual(rotation, 270f, 1f))
			ins = InstructionFactory.instance.getInstruction("Right");
		else
			ins = InstructionFactory.instance.getInstruction("Up");
		return ins;
	}

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		return getInstruction(bot).runDefault(worldController, bot);
	}

	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		return getInstruction(bot).runInstantly(worldController, bot);
	}
}
