package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class DownInstruction extends MovementInstruction implements RotableInstruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		return runDefault(worldController, bot, 0, -1);
	}
	
	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		return runInstantly(worldController, bot, 0, -1);
	}
}
