package com.violentbits.ageo.apparat;

import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.instructions.Instruction;

/**
 * The set of instructions that a bot can execute. Each instruction is
 * stored in a ReadOnlyCell.
 */
public class InstructionSet {
	private Array<ReadOnlyCell> instructions = new Array<ReadOnlyCell>();
	
	public InstructionSet() {
		
	}
	
	public InstructionSet(Instruction... instructions) {
		ReadOnlyCell cell;
		for (Instruction ins : instructions) {
			cell = new ReadOnlyCell();
			cell.put(ins);
			this.instructions.add(cell);
		}
	}
	
	public Array<ReadOnlyCell> getCells() {
		return instructions;
	}
}
