package com.violentbits.ageo.apparat;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.io.Assets;

public class ProgramTable extends Table implements ProgramListener {
	@SuppressWarnings("unused")
	private static final String TAG = ProgramTable.class.getName();

	private final WorldController worldController;
	private final Table buttonRow;
	private final TextButton hideButton;
	private DragAndDrop dragAndDrop;
	private Bot bot;

	private ClickListener cellClicked = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			CellActor cellActor = (CellActor) event.getListenerActor();
			if (!cellActor.isDisabled() && getTapCount() == 2) {
				if (!worldController.isRunning() && bot.editable) {
					if (cellActor.getCell().remove() != null)
						SoundController.instance.play(Assets.instance.getSound("select.wav"));
				} else {
					SoundController.instance.play(Assets.instance.getSound("no.ogg"));
				}
			}
		}
	};
	
	private void setCell(AbstractCell cell, boolean editable) {
		CellActor cellActor = new CellActor(worldController, cell);
		cellActor.setSize(Constants.INSTRUCTION_ICON_SIZE, Constants.INSTRUCTION_ICON_SIZE);
		buttonRow.add(cellActor).size(Constants.INSTRUCTION_ICON_SIZE);

		if (editable) {
			dragAndDrop.addSource(new CellSource(worldController, cellActor));
			dragAndDrop.addTarget(new CellTarget(cellActor));
		}
	}

	private void setCells(Array<? extends AbstractCell> cells, boolean editable) {
		// Check if all buttons fit in a single row
		int nRows = getStage().getWidth()<cells.size*Constants.INSTRUCTION_ICON_SIZE+hideButton.getWidth()?2:1;
		int nCols = nRows>1?cells.size/2:cells.size;
		int i;
		
		buttonRow.clear();
		for (i=0; i<nCols; i++) {
			setCell(cells.get(i), editable);
		}
		if (nRows>1) {
			buttonRow.row();
			for (;i<cells.size; i++) {
				setCell(cells.get(i), editable);
			}
		}
		buttonRow.pack();
	}

	public void setBot(Bot bot) {
		if (this.bot != bot) {
			if (this.bot != null)
				this.bot.getProgram().removeListener(this);
			this.bot = bot;
			if (!Constants.IS_SOLVER_ACTIVE)
				bot.getProgram().addListener(this);
			setCells(bot.getProgram().getCells(), bot.editable);
			createClickListeners();
		}
	}

	public ProgramTable(WorldController worldController, DragAndDrop dragAndDrop) {
		super(Assets.instance.skin);
		this.worldController = worldController;
		this.dragAndDrop = dragAndDrop;
		hideButton = new TextButton("<<", Assets.instance.skin);
		hideButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if ((hideButton.getText() + "").equals("<<")) {
					hideButton.setText(">>");
					addAction(Actions.moveBy(-buttonRow.getWidth(), 0, 0.4f));
				} else {
					hideButton.setText("<<");
					addAction(Actions.moveTo(0, 0, 0.4f));
				}
				setVisible(true);
			}
		});
		

		dragAndDrop.setDragActorPosition(-Constants.INSTRUCTION_ICON_SIZE / 2, Constants.INSTRUCTION_ICON_SIZE / 2);

		buttonRow = new Table();
		add(buttonRow).fillX();
		add(hideButton).height(Constants.INSTRUCTION_ICON_SIZE);
		pack();
		setFillParent(true);
		setVisible(true);
		left().bottom();
	}

	private void createClickListeners() {
		for (Actor actor : buttonRow.getChildren()) {
			if (actor instanceof CellActor) {
				actor.addListener(cellClicked);
			}
		}
	}

	@Override
	public void executed(Bot bot, Instruction instruction, int counter) {
		Actor actor = buttonRow.getChildren().get(counter);
		actor.addAction(sequence(fadeOut(worldController.actionTime / 2f), fadeIn(worldController.actionTime / 2f)));
		String soundName = instruction.getSoundName();
		if (soundName != null) {
			SoundController.instance.play(Assets.instance.getSound(soundName));
		}
	}
	
	public CellActor getCellActor(int index) {
		return (CellActor)buttonRow.getChildren().get(index);
	}
	
	public void setDisabled(boolean isDisabled) {
		for (Actor actor : buttonRow.getChildren()) {
			if (actor instanceof CellActor) {
				CellActor cellActor = (CellActor) actor;
				cellActor.setDisabled(isDisabled);
				cellActor.setTouchable(isDisabled?Touchable.disabled:Touchable.enabled);
			}
		}
	}
	
	public void clearCells() {
		if (!worldController.isRunning() && bot.editable) {
			for (int i = buttonRow.getChildren().size-1; i>=0; i--) {
				Actor actor = buttonRow.getChildren().get(i);
				if (actor instanceof CellActor) {
					CellActor cellActor = (CellActor) actor;
					cellActor.getCell().remove();
				}
			}
		}
	}
}
