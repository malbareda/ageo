package com.violentbits.ageo.apparat;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.after;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.color;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.hide;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.repeat;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.show;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.DirectionsHelper;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.instructions.RotableInstruction;
import com.violentbits.ageo.io.Assets;

public class CellActor extends ImageButton implements CellListener {
	private AbstractCell cell;
	private WorldController worldController;

	public CellActor(WorldController worldController, AbstractCell cell) {
		super(createStyle(cell));
		this.cell = cell;
		getChildren().get(0).setOrigin(Constants.INSTRUCTION_ICON_SIZE/2f, Constants.INSTRUCTION_ICON_SIZE/2f);
		this.worldController = worldController;
		if (cell instanceof Cell && cell.getInstruction() instanceof RotableInstruction) {
			getChildren().get(0).addAction(rotateTo(-DirectionsHelper.getCurrentRotation(worldController)));
		}
		if (!Constants.IS_SOLVER_ACTIVE)
			cell.addListener(this);
	}

	private static ImageButtonStyle createStyle(AbstractCell cell) {
		TextureRegion image;
		if (cell.getInstruction() != null) {
			image = Assets.instance.get(cell.getInstruction().getName());
		} else {
			// we have a special "void" region in our atlas file, which is just
			// black
			image = Assets.instance.get("void");
		}
		ImageButtonStyle style = new ImageButtonStyle();
		TextureRegionDrawable imageUp = new TextureRegionDrawable(image);
		style.imageUp = imageUp;
		style.imageUp.setMinHeight(Constants.INSTRUCTION_ICON_SIZE);
		style.imageUp.setMinWidth(Constants.INSTRUCTION_ICON_SIZE);
		style.imageDown = imageUp.tint(Constants.LIGHT_GRAY);
		style.imageDown.setMinHeight(Constants.INSTRUCTION_ICON_SIZE);
		style.imageDown.setMinWidth(Constants.INSTRUCTION_ICON_SIZE);
		style.imageDisabled = imageUp.tint(Constants.GRAY);
		style.imageDisabled.setMinHeight(Constants.INSTRUCTION_ICON_SIZE);
		style.imageDisabled.setMinWidth(Constants.INSTRUCTION_ICON_SIZE);
		return style;
	}

	@Override
	public void changed(AbstractCell cell, Instruction oldValue, Instruction newValue) {
		getChildren().get(0).addAction(after(rotateTo(0)));
		setStyle(createStyle(cell));
	}

	public AbstractCell getCell() {
		return cell;
	}

	@Override
	public void event(CellEvent event, AbstractCell cell, Object eventInfo) {
		Actor actor;
		switch (event) {
		case DELETE:
		case COPY:
			actor = getChildren().get(0);
			actor.addAction(sequence(
					parallel(scaleTo(0.1f, 0.1f, worldController.actionTime / 2f),
							Actions.rotateBy(360 * 4, worldController.actionTime / 2f)),
					parallel(scaleTo(1f, 1f, worldController.actionTime / 2f),
							rotateTo(0, worldController.actionTime / 2f))));
			break;
		case SWAP:
			actor = getChildren().get(0);
			actor.addAction(sequence(Actions.moveBy(5, 0, worldController.actionTime / 8f),
					repeat(2,
							sequence(Actions.moveBy(-10, 0, worldController.actionTime / 4f),
									Actions.moveBy(10, 0, worldController.actionTime / 4f))),
					Actions.moveBy(-5, 0, worldController.actionTime / 8f)));
			break;
		case TRANSFER:
			actor = getChildren().get(0);
			Color color = actor.getColor();
			actor.addAction(sequence(color(Color.BLUE, worldController.actionTime / 2f),
					color(color, worldController.actionTime / 2f)));
			break;
		case REPEAT:
			actor = getChildren().get(0);
			actor.addAction(repeat(3, sequence(hide(), delay(worldController.actionTime / 16), show(),
					delay(worldController.actionTime / 16))));
			break;
		case ROTATE:
			actor = getChildren().get(0);
			int rotation = (Integer)eventInfo;
			actor.addAction(sequence(Actions.rotateTo(-rotation),Actions.rotateTo(0, worldController.actionTime*0.9f)));
			break;
		case ROTATE_INSTANTLY:
			actor = getChildren().get(0);
			actor.addAction(Actions.rotateBy((Float)eventInfo));
			break;
		default:
			break;
		}

	}

}
