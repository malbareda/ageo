package com.violentbits.ageo.apparat;

import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.instructions.Instruction;

/**
 * A memory cell where a program instruction can be stored. Subclasses define the
 * cell behavior when data is written to or read from the cell.
 */
public abstract class AbstractCell {
	/**
	 * The instruction stored in this cell. Can be null.
	 */
	protected Instruction instruction;
	/**
	 * The listeners that get notified whenever the contents of the cell change.
	 */
	protected Array<CellListener> listeners = new Array<CellListener>();
    /**
     * Adds a listener that will be notified when the contents of this cell change.
     * 
     * @param listener  The CellListener to add.
     */
	public void addListener(CellListener listener) {
    	listeners.add(listener);
    }
	/**
	 * Notify all listener of a contents change, only if there has been really a change.
	 * Subclasses of this class have to call this method when they change the value of
	 * the instruction attribute.
	 * 
	 * @param oldInstruction  The instruction stored by this cell before the last assignment.
	 */
	protected void notifyListeners(Instruction oldInstruction) {
		for (CellListener listener : listeners)
    		listener.changed(this, oldInstruction, instruction);
	}
	
	protected void notifyEvent(CellEvent event) {
		notifyEvent(event, null);
	}
	
	protected void notifyEvent(CellEvent event, Object eventInfo) {
		for (CellListener listener : listeners)
			listener.event(event, this, eventInfo);
	}
	/**
	 * Returns if this cell stores an instruction.
	 * 
	 * @return true if there is an instruction, false otherwise.
	 */
	public boolean isEmpty() {
        return instruction == null;
    }
	/**
	 * Tells the cell to store an instruction.
	 * 
	 * @param instruction  A non-null instruction.
	 * @return true if the instruction has been stored, false otherwise. In a cell sequence,
	 * the instruction can be stored in another cell. This is method returns true if the
	 * instruction has been stored in any cell.
	 */
	public abstract boolean put(Instruction instruction);
	/**
	 * Tells the cell to remove the stored instruction, if any. Implementation can ignore this
	 * petition if the cell policy doesn't allow to store instructions.
	 * 
	 * @return the instruction stored so far in the cell, or null if there wasn't any instruction
	 * stored.
	 */
	public abstract Instruction remove();
	/**
	 * Retrieves the stored instruction.
	 * 
	 * @return the stored instruction or null if there isn't any instruction.
	 */
    public Instruction getInstruction() {
        return instruction;
    }

    @Override
    public String toString() {
        return getClass().getName()+ "[" + instruction + "]";
    }
}
