package com.violentbits.ageo.tutorial;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class IntTutorialStep extends TutorialStep {
	private int n;

	public IntTutorialStep(Actor target, int n, TutorialEvent event) {
		super(target, event);
		this.n = n;
	}

	public int getInt() {
		return n;
	}
}
