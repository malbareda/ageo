package com.violentbits.ageo.tutorial;

import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.apparat.CellActor;
import com.violentbits.ageo.game.HudRenderer;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.tutorial.TutorialStep.TutorialEvent;

/**
 * This is a helper class that build the suitable tutorial steps given the strings with the instructions.
 * 
 * The following strings can be provided:
 * 
 * - play: a tutorial step that waits until the player clicks on the play button.
 * - stop: a tutorial step that waits until the player clicks on the stop button.
 * - win: a tutorial step that waits until the level is won.
 * - remove#<num>: a tutorial step that waits until the instruction number <num> is removed in the program table.
 * - nsteps#<num>: a tutorial step that waits <num> steps.
 * - selectbot#<num>: a tutorial step that waits until the user selects the bot with <num> index.
 * - <Instruction>: a tutorial step that waits until the user adds the specified instruction in the program table.
 *
 */
public class TutorialFactory {
	
	private TutorialFactory() {}
	
	public static Array<TutorialStep> parseTutorial(String[] ss, WorldController worldController, HudRenderer renderer) {
		Array<TutorialStep> steps = new Array<TutorialStep>();
		for (String s : ss) {
			steps.add(parseStep(s, worldController, renderer));
		}
		return steps;
	}

	public static TutorialStep parseStep(String s, WorldController worldController, HudRenderer renderer) {
		TutorialStep step=null; 
		if (s.equals("play")) {
			step = new TutorialStep(renderer.getBtnPlay(), TutorialEvent.ON_PLAY);
		} else if (s.equals("fastforward")) {
			step = new TutorialStep(renderer.getBtnFastForward(), TutorialEvent.ON_PLAY);
		} else if (s.equals("stop")) {
			step = new TutorialStep(renderer.getBtnStop(), TutorialEvent.ON_STOP);
		} else if (s.equals("win")) {
			step = new TutorialStep(null, TutorialEvent.ON_WIN);
		} else if (s.equals("helpinfo")) {
			step = new HelpInfoTutorialStep(renderer);
		} else if (s.startsWith("remove")) {
			int pos=Integer.parseInt(s.split("#")[1]);
			CellActor cellActor = renderer.getProgramTable().getCellActor(pos);
			step = new InstructionRemovedTutorialStep(cellActor);
		} else if (s.startsWith("nsteps")) {
			step = new IntTutorialStep(null, Integer.parseInt(s.split("#")[1]), TutorialEvent.ON_NSTEPS);
		} else if (s.startsWith("selectbot")) {
			int pos = Integer.parseInt(s.split("#")[1]);
			step = new IntTutorialStep(worldController.getBots().get(pos), pos, TutorialEvent.ON_BOT_SELECTED);
		} else {
			step = new InstructionTutorialStep(renderer, s);
		}
		return step;
	}
}
