package com.violentbits.ageo.tutorial;

import static com.badlogic.gdx.math.Interpolation.bounceOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.io.Assets;

/**
 * Arrows used in tutorials. These arrows point to another actor in the game.
 */
public class Arrow extends Image {
	/**
	 * Constructor.
	 * 
	 * @param target  Actor to point to.
	 */
	public Arrow(Actor target) {
		super(Assets.instance.get("arrow"));
		Stage stage = target.getStage();
		setSize(getWidth()*stage.getWidth()/Constants.LEVEL_SCREEN_WIDTH, getHeight()*stage.getHeight()/Constants.LEVEL_SCREEN_HEIGHT);
		setOrigin(getWidth()/2f, getHeight()/2f);
		Vector2 pos = target.localToStageCoordinates(new Vector2(0,0)); 
		float x = pos.x;
		float y = pos.y;
		float h = stage.getHeight();
		float w = stage.getWidth();
		float dx=0, dy=0;
		if (x>3f*w/4f) {
			setRotation(270f);
			setPosition(x-getWidth(), y+(target.getHeight()-getHeight())/2f);
			dx=-getWidth();
		} else if (y<h/4f) { 
			setRotation(180f);
			setPosition(x+(target.getWidth()-getWidth())/2f, y+target.getHeight());
			dy=getHeight();
		} else if (y>3f*h/4f) {
			setRotation(0f);
			setPosition(x+(target.getWidth()-getWidth())/2f, y-target.getHeight());
			dy=-getHeight();
		} else {
			setRotation(90f);
			setPosition(x+target.getWidth(), y+(target.getHeight()-getHeight())/2f);
			dx=getWidth();
		}
		addAction(
			forever(sequence(
				Actions.moveBy(dx, dy, 0.8f),
				Actions.moveBy(-dx, -dy, 0.8f, bounceOut)
			)
		));
		stage.addActor(this);
		
	}
}
