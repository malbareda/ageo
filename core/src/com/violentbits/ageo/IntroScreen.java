package com.violentbits.ageo;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.violentbits.ageo.game.GameScreen;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.io.LevelInfo;
import com.violentbits.ageo.scene.SoundAction;

/**
 * This screen show a dialog preconfigured in the levels file.
 */
public class IntroScreen extends AbstractGameScreen {
	private static final float w = Constants.LEVEL_SCREEN_WIDTH;
	private static final float h = Constants.LEVEL_SCREEN_HEIGHT;
	private Stage stage;
	private LevelInfo levelInfo;
	private Label[] labels;
	private TextButton startButton;
	private LabelStyle styleLeft, styleRight;
	private SequenceAction action;

	public IntroScreen(AgeoGame game, LevelInfo levelInfo) {
		super(game);
		this.levelInfo = levelInfo;
	}

	@Override
	public void show() {
		Gdx.input.setCatchBackKey(true);
		Gdx.graphics.setContinuousRendering(false);
	    Gdx.graphics.requestRendering();
		GamePreferences.instance.load();
		stage = new Stage(
				new StretchViewport(w, h));
		Gdx.input.setInputProcessor(stage);
		buildStage();
	}
	
	private void createStyles() {
		Skin skin = Assets.instance.skin;
		styleLeft = new LabelStyle(skin.getFont("default-font"), Color.WHITE);
		styleLeft.background = new NinePatchDrawable(Assets.instance.getNinePatch("dialog_left"));
		styleLeft.background.setRightWidth(25);
		styleLeft.background.setLeftWidth(25);
		
		styleRight = new LabelStyle(skin.getFont("default-font"), Color.YELLOW);
		styleRight.background = new NinePatchDrawable(Assets.instance.getNinePatch("dialog_right"));
		styleRight.background.setLeftWidth(25);
		styleRight.background.setRightWidth(25);
	}
	
	private void createStartButton() {
		startButton = new TextButton("Skip", Assets.instance.skin);
		startButton.setSize(Constants.BUTTON_WIDTH, Constants.BUTTON_HEIGHT);
		startButton.setPosition((w-startButton.getWidth())/2, Constants.HUD_MARGIN);
		startButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Assets.instance.loadLevel(levelInfo);
				game.setScreen(new GameScreen((AgeoGame)game));
			}
		});
		stage.addActor(startButton);
	}
	
	private void createLabels() {
		final String[] messages = levelInfo.messages;
		final boolean[] left = levelInfo.left;
		String[] soundNames = levelInfo.soundNames;
		
		labels = new Label[messages.length];
		for (int i=0; i<messages.length; i++) {
			Label label = new Label(messages[i], left[i]?styleLeft:styleRight);
			label.setPosition(left[i]?-label.getWidth():w+label.getWidth(), h-60*(i+1));
			stage.addActor(label);
			labels[i]=label;
			Action move = moveTo(left[i]?w/6:5*w/6-label.getWidth(), h-60*(i+1), 1f);
			move.setTarget(label);
			action.addAction(move);
			SoundAction sound = action(SoundAction.class);
			sound.setSoundName(soundNames[i]);
			action.addAction(sound);
			if (i!=messages.length-1)
				action.addAction(delay(1f));
		}
	}
	
	private void buildStage() {
		createStyles();
		createStartButton();
		action = sequence();
		createLabels();
		action.addAction(Actions.run(new Runnable() {
			@Override
			public void run() {
				startButton.setText("Start");
			}
		}));
		stage.addAction(action);
		createStageListener();
	}
	
	private void createStageListener() {
		stage.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				stage.getRoot().clearActions();
				for (int i=0; i<levelInfo.messages.length; i++) {
					labels[i].setPosition(levelInfo.left[i]?w/6:5*w/6-labels[i].getWidth(), h-60*(i+1));
				}
				startButton.setText("Start");
			}
		});
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(Constants.DARK_BLUE.r, Constants.DARK_BLUE.g, Constants.DARK_BLUE.b, Constants.DARK_BLUE.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		stage.dispose();
	}

}
