package com.violentbits.ageo.io;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.violentbits.ageo.io.LevelStage;

public class LevelInfoLoader extends SynchronousAssetLoader<LevelStageArray, LevelInfoLoader.LevelInfoLoaderParameter> {

	public LevelInfoLoader(FileHandleResolver resolver) {
		super(resolver);
	}

	public static class LevelInfoLoaderParameter extends AssetLoaderParameters<LevelStageArray> {
	}

	@SuppressWarnings("unchecked")
	@Override
	public LevelStageArray load(AssetManager assetManager, String fileName, FileHandle file,
			LevelInfoLoaderParameter parameter) {
		Json json = new Json();
		json.addClassTag("stage", LevelStage.class);
		json.addClassTag("level", LevelInfo.class);
		LevelStageArray levelStageArray = new LevelStageArray();
		levelStageArray.levelStages = json.fromJson(Array.class, file);
		for (LevelStage stage : levelStageArray.levelStages) {
			stage.levels = json.fromJson(Array.class, Gdx.files.internal(stage.index));
		}
		return levelStageArray;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file,
			LevelInfoLoaderParameter parameter) {
		return null;
	}
}
