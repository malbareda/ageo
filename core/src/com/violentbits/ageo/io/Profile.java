package com.violentbits.ageo.io;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;

public class Profile {
	private Preferences currentGame;
	
	private String levelName;
	private String name;
	private boolean won;
	private int nProfile;
	private int tries;
	private int steps;
	private int inst;
	private int instStars;
	private int stepsStars;
	private int bestSteps;
	private int bestInst;
	private int bestInstStars;
	private int bestStepsStars;
	
	private int levelPos;
	private int currentStage;
	
	public Profile(int nProfile) {
		this.nProfile = nProfile;
		if (nProfile>=0) {
			currentGame = Gdx.app.getPreferences("savedgame"+nProfile);
			levelPos = currentGame.getInteger("currentLevelPos", 0);
			currentStage = currentGame.getInteger("currentStage", 0);
			name = currentGame.getString("name", "");
			// Check if levelPos and currentStage have valid values
			Array<LevelStage> stages = Assets.instance.getStages();
			if (currentStage<0 || currentStage>=stages.size) {
				currentStage=0;
			}
			LevelStage levelStage = stages.get(currentStage);
			Array<LevelInfo> levels = levelStage.levels;
			if (levelPos<0 || levelPos>=levels.size) {
				levelPos=0;
			}
		} else {
			levelPos = 0;
			currentStage = 0;
			name = "";
		}
	}
	
	public int getNProfile() {
		return nProfile;
	}
	
	public String getLevelName() {
		return levelName;
	}
	public boolean isWon() {
		return won;
	}
	public boolean isWon(String levelName) {
		return currentGame.getBoolean(levelName, false);
	}
	public int getTries() {
		return tries;
	}
	public int getSteps() {
		return steps;
	}
	public int getInst() {
		return inst;
	}
	public int getInstStars() {
		return instStars;
	}
	public int getStepsStars() {
		return stepsStars;
	}
	public int getBestSteps() {
		return bestSteps;
	}
	public int getBestInst() {
		return bestInst;
	}
	public int getBestStepsStars() {
		return bestStepsStars;
	}
	public int getBestStepsStars(String levelName) {
		return currentGame.getInteger(levelName+".stepsstars", -1);
	}
	public int getBestInstStars() {
		return bestInstStars;
	}
	public int getBestInstStars(String levelName) {
		return currentGame.getInteger(levelName+".insstars", -1);
	}
	public int getLevelPos() {
		return levelPos;
	}
	public int getCurrentStage() {
		return currentStage;
	}
	public String getName() {
		return name;
	}
	
	public void loadLevel(String levelName) {
		if (this.levelName != levelName) {
			this.levelName = levelName;
			won = currentGame.getBoolean(levelName, false);
			tries = currentGame.getInteger(levelName+".tries", 0);
			bestSteps = currentGame.getInteger(levelName+".steps", 0);
			bestInst = currentGame.getInteger(levelName+".ins", 0);
			bestInstStars = currentGame.getInteger(levelName+".insstars",-1);
			bestStepsStars = currentGame.getInteger(levelName+".stepsstars", -1);
			steps = bestSteps;
			inst = bestInst;
			instStars = bestInstStars;
			stepsStars = bestStepsStars;
		}
	}
	
	private void saveLevel() {
		currentGame.putBoolean(levelName, won);
		currentGame.putInteger(levelName+".tries", tries);
		currentGame.putInteger(levelName+".steps", bestSteps);
		currentGame.putInteger(levelName+".ins", bestInst);
		currentGame.putInteger(levelName+".insstars", bestInstStars);
		currentGame.putInteger(levelName+".stepsstars", bestStepsStars);
		currentGame.flush();
	}
	
	public void setLevelPos(int levelPos) {
		this.levelPos=levelPos;
		currentGame.putInteger("currentLevelPos", levelPos);
		currentGame.flush();
	}
	
	public void setCurrentStage(int currentStage) {
		this.currentStage=currentStage;
		currentGame.putInteger("currentStage", currentStage);
		setLevelPos(0);
	}
	
	public void setName(String name) {
		this.name = name;
		currentGame.putString("name", name);
		currentGame.flush();
	}
	
	public void levelTried() {
		tries++;
		saveLevel();
	}
	
	public void levelWon(int minSteps, int minInst, int maxInst, int nSteps, int nInst) {
		won = true;
		steps = nSteps;
		stepsStars=0;
		if (steps <= minSteps) {
			stepsStars++;
		}
		if (steps <= 5*minSteps/4) {
			stepsStars++;
		}
		if (steps <= 3*minSteps/2) {
			stepsStars++;
		}
		if (steps<bestSteps || bestStepsStars<0) {
			bestSteps = steps;
			bestStepsStars=stepsStars;
		}
		inst=nInst;
		instStars=0;
		if (inst <= minInst) {
			instStars++;
		}
		if (inst <= (2*minInst+maxInst)/3) {
			instStars++;
		}
		if (inst <= (minInst+2*maxInst)/3) {
			instStars++;
		}
		if (inst<bestInst || bestInstStars<0) {
			bestInst = inst;
			bestInstStars=instStars;
		}
		saveLevel();
	}
	
	public void delete() {
		currentGame.clear();
		currentGame.flush();
		levelName="";
		name="";
		won=false;
		tries=0;
		steps=0;
		inst=0;
		instStars=-1;
		stepsStars=-1;
		bestSteps=0;
		bestInst=0;
		bestInstStars=-1;
		bestStepsStars=-1;
		levelPos=0;
		currentStage=0;
	}
}
