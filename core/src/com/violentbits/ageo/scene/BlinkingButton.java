package com.violentbits.ageo.scene;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.color;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.violentbits.ageo.Constants;

public class BlinkingButton extends Button {
	private boolean isBlinking = false;
	
	public void startBlink() {
		if (!isBlinking) {
			addAction(forever(sequence(color(Constants.PINK, 1.5f),color(Color.WHITE, 1.5f))));
			isBlinking = true;
		}
	}
	
	public void stopBlink() {
		if (isBlinking) {
			clearActions();
			addAction(color(Color.WHITE, 1.5f));
			isBlinking = false;
		}
	}

	public BlinkingButton() {
		super();
	}

	public BlinkingButton(Actor child, ButtonStyle style) {
		super(child, style);
	}

	public BlinkingButton(Actor child, Skin skin, String styleName) {
		super(child, skin, styleName);
	}

	public BlinkingButton(Actor child, Skin skin) {
		super(child, skin);
	}

	public BlinkingButton(ButtonStyle style) {
		super(style);
	}

	public BlinkingButton(Drawable up, Drawable down, Drawable checked) {
		super(up, down, checked);
	}

	public BlinkingButton(Drawable up, Drawable down) {
		super(up, down);
	}

	public BlinkingButton(Drawable up) {
		super(up);
	}

	public BlinkingButton(Skin skin, String styleName) {
		super(skin, styleName);
	}

	public BlinkingButton(Skin skin) {
		super(skin);
	}
}
