package com.violentbits.ageo.scene;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.io.Assets;

public class SelectedActor extends Image {
	private Bot selectedBot;
	private boolean tracking;
	private float accDelta;
	private float oldX, oldY;

	public SelectedActor() {
		super(Assets.instance.get("selected"));
		setSize(1.2f, 1.2f);
		setOrigin(getWidth()/2f, getHeight()/2f);
		getColor().a = 0.6f;
		setVisible(false);
	}
	
	public void setSelectedBot(Bot bot) {
		if (bot != selectedBot && bot != null) {
			selectedBot = bot;
			if (!isVisible()) {
				setTracking(true);
				setVisible(true);
			} else {
				setTracking(false);
				oldX = getX();
				oldY = getY();
				accDelta = 0;
			}
		}		
	}
	
	private void setTracking(boolean tracking) {
		if (this.tracking != tracking) {
			this.tracking = tracking;
			if (tracking==true) {
				addAction(Actions.repeat(3, Actions.sequence(
					Actions.scaleTo(1.2f, 1.2f, 0.1f),
					Actions.scaleTo(1f, 1f, 0.1f)
				)));
			}
		}
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		
		if (selectedBot!=null) {
			float newX = selectedBot.getX()+(selectedBot.getWidth()-getWidth())/2f;
			float newY = selectedBot.getY()+(selectedBot.getHeight()-getHeight())/2f;
			if (tracking) {
				addAction(Actions.moveTo(newX, newY));
			} else {
				accDelta +=delta;
				addAction(Actions.moveTo(
						MathUtils.lerp(oldX, newX, accDelta*2f),
						MathUtils.lerp(oldY, newY, accDelta*2f)));
				if (accDelta>=0.5f)
					setTracking(true);
			}
		}
	}
}
