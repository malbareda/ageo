package com.violentbits.ageo.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.violentbits.ageo.AgeoGame;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.game.GameScreen;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.io.Profile;
import com.violentbits.ageo.scene.Indications.Dir;

public class StatsWindow extends Window {
	private int nInst=0;
	private int nSteps;
	private Image[] imgStarInsts = new Image[3];
	private Image[] imgStarSteps = new Image[3];
	private Image imgInst, imgNSteps;
	private Label lblInst, lblNSteps, lblInstBest, lblStepsBest;
	private Button btnHelp, btnRetry, btnNext;
	private WorldController worldController;

	public StatsWindow(WorldController worldController) {
		super("Level stats", Assets.instance.skin);
		this.worldController=worldController;
		createImages();
		setStats();
		setBestStats();
		addActors();
		addButtons();
		pack();
		lightUpStars();
	}
	
	private void addButtons() {
		btnHelp = new Button(Assets.instance.createButtonStyle("help"));
		add(btnHelp).left().colspan(3).size(Constants.INSTRUCTION_ICON_SIZE);
		btnHelp.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				final Indications indications = new Indications(getStage(), getSkin());
				indications.addLabelAndLine(imgInst, "Number of instructions used", Dir.RIGHT, 250f);
				indications.addLabelAndLine(imgNSteps, "Number of steps executed", Dir.RIGHT, 250f);
				indications.addLabelAndLine(btnRetry, "Replay this level", Dir.UP, 150f);
				indications.addLabelAndLine(btnNext, "Select another level", Dir.UP, 100f);
				getStage().addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						indications.removeAll();
						getStage().removeListener(this);
					}
				});
			}
		});
		btnRetry = new Button(Assets.instance.createButtonStyle("stop"));
		add(btnRetry).size(Constants.INSTRUCTION_ICON_SIZE);
		btnRetry.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						Assets.instance.loadLevel(Assets.instance.getLevelInfo());
						AgeoGame game = ((AgeoGame)Gdx.app.getApplicationListener());
						game.setScreen(new GameScreen(game));
					}
			    });
			}
		});
		btnNext = new Button(Assets.instance.createButtonStyle("play"));
		add(btnNext).size(Constants.INSTRUCTION_ICON_SIZE);
		btnNext.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				worldController.returnToLevelScreen();
			}
		});
	}
	
	private void addActors() {
		add(imgInst);
		add(lblInst);
		for (int i=0; i<imgStarInsts.length; i++) {
			add(imgStarInsts[i]);
		}
		row();
		if (lblInstBest!=null) {
			add(lblInstBest).colspan(5).row();
		}
		add(imgNSteps);
		add(lblNSteps);
		for (int i=0; i<imgStarSteps.length; i++) {
			add(imgStarSteps[i]);
		}
		row();
		if (lblStepsBest!=null) {
			add(lblStepsBest).colspan(5).row();
		}
	}
	
	private void setBestStats() {
		Profile profile = GamePreferences.instance.getCurrentProfile();
		int bestInst = profile.getBestInst();
		if (bestInst>0)
			lblInstBest = new Label("Your best: "+bestInst, getSkin());
		int bestNSteps = profile.getBestSteps();
		if (bestNSteps>0)
			lblStepsBest = new Label("Your best: "+bestNSteps, getSkin());
	}
	
	private void setStats() {
		Profile profile = GamePreferences.instance.getCurrentProfile();
		nSteps=profile.getSteps();
		nInst=profile.getInst();
		lblInst = new Label(""+nInst, getSkin(), "title");
		lblNSteps = new Label(""+nSteps, getSkin(), "title");
	}
	
	private void createImages() {
		imgInst = new Image(Assets.instance.get("gears"));
		imgInst.setColor(Constants.PINK);
		imgNSteps = new Image(Assets.instance.get("clock"));
		imgNSteps.setColor(Constants.PINK);
		for (int i=0; i<imgStarInsts.length; i++) {
			imgStarInsts[i] = new Image(Assets.instance.get("star"));
			imgStarInsts[i].setColor(Constants.GRAY);
			imgStarSteps[i] = new Image(Assets.instance.get("star"));
			imgStarSteps[i].setColor(Constants.GRAY);
		}
	}

	private void lightUpStars() {
		Profile profile = GamePreferences.instance.getCurrentProfile();
		int instStars = profile.getInstStars();
		int stepsStars = profile.getStepsStars();
		
		for (int i=0; i<instStars; i++) {
			imgStarInsts[i].addAction(Actions.color(Constants.PURPLE, 1f));
		}
		for (int i=0; i<stepsStars; i++) {
			imgStarSteps[i].addAction(Actions.color(Constants.PURPLE, 1f));
		}
	}
}
