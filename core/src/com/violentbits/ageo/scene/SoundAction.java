package com.violentbits.ageo.scene;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.io.Assets;

public class SoundAction extends Action {
	private String soundName;
	
	public void setSoundName(String soundName) {
		this.soundName = soundName;
	}
	
	@Override
	public boolean act(float delta) {
		SoundController.instance.play(Assets.instance.getSound(soundName));
		return true;
	}
}
