package com.violentbits.ageo.solver;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.AgeoGame;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class Solver extends WorldController {
	private static final String TAG=Solver.class.getName();
	private Array<SolverProgram> solverPrograms;
	private int minSteps;
	private Array<String> cps = new Array<String>(false, 100);
	private boolean next;
	private int solverIndex;
	private long initialTime;
	private long lastTime;
	private long currentTime;
	
	private Array<int[]> combinations;
	private int nComb;
	
	public Solver(AgeoGame game) {
		super(game);
	}
	
	@Override
	public void init() {
		super.init();
		lastTime=initialTime=System.currentTimeMillis();
		solverPrograms = new Array<SolverProgram>();
		minSteps=Constants.SOLVER_LIMIT;
		Array<Bot> bots = getBots();
		for (Bot b: bots) {
			if (b.editable)
				solverPrograms.add(new SolverProgram(b));
		}
		solverIndex=solverPrograms.size-1;
		combinations=Combinator.findCombinations(solverPrograms);
		resetSolvers();
		run(0);
	}
	
	private void resetSolvers() {
		for (int i=0; i<solverPrograms.size; i++) {
			solverPrograms.get(i).setProgramSize(combinations.get(nComb)[i]);
		}
		nComb++;
	}
	
	@Override
	public void action() {
		super.action();
		if (getNSteps()>=minSteps) {
			next=true;
		}
		if (next) {
			stop();
		} else {
			if (lost()) {
				next=true;
			} else if (getBots().get(getCurrentBotIndex())==solverPrograms.get(solverIndex).getBot()) {
				String cp = GameState.computeGameState(this);
				if (cps.contains(cp, false)) {
					next=true;
				} else
					cps.add(cp);
			}
		}
	}
	
	@Override
	public void stop() {
		super.stop();
		currentTime=System.currentTimeMillis();
		if (currentTime-lastTime>1800000) {
			Gdx.app.debug(TAG, "Elapsed time: "+(currentTime-initialTime)/60000+" min");
			lastTime=currentTime;
		}
		
		if (next) {
			next=false;
			cps.clear();
			
			while (solverIndex>=0 && solverPrograms.get(solverIndex).computeNextCandidate()==false) {
				solverPrograms.get(solverIndex).resetProgram();
				solverIndex--;
			}
			if (solverIndex>=0) {
				solverIndex=solverPrograms.size-1;
				run(0);
			} else {
				if (nComb<combinations.size) {
					solverIndex=solverPrograms.size-1;
					resetSolvers();
					run(0);
				} else {
					Gdx.app.debug(TAG, "Finished ");
				}
			}
		}
	}
	
	@Override
	public void update(float delta) {
		if (isRunning() && !hasWon()) {
			action();
		}
	}
	
	private boolean lost() {
		for (Bot bot : getBots()) {
			if (bot.editable && !bot.isActive())
				return true;
		}
		return false;
	}
	
	@Override
	protected void win() {
		if (getNSteps()<minSteps) {
			String log="Solution found ("+getNSteps()+") ";
		
			for (int i=0; i<solverPrograms.size; i++) {
				SolverProgram solverProgram = solverPrograms.get(i);
				log+=i+"("+solverProgram.getProgramSize()+"): ";
				solverProgram.restoreBackup();
				log+=solverProgram.programToString();
			}
			Gdx.app.log(TAG, log);
			if (getNSteps() < minSteps)
				minSteps=getNSteps();
		}
		next=true;
	}
	
	@Override
	public boolean keyUp(int keycode) {
		super.keyUp(keycode);
		if (keycode == Keys.SPACE) {
			String log = "Trying: ";
			
			for (int i=0; i<solverPrograms.size; i++) {
				log+=solverPrograms.get(i).programToString()+"  ";
			}
			Gdx.app.debug(TAG, log);
		}
		return false;
	}
}
