package com.violentbits.ageo.solver;

import java.util.Comparator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Sort;
import com.violentbits.ageo.Constants;

public class Combinator {

	private Combinator() {}
	
	private static int sum(int[] a) {
		int s=0;
		for (int i:a){
			s+=i;
		}
		return s;
	}
	
	public static Array<int[]> findCombinations(Array<SolverProgram> solvers) {
		int minInst=Constants.SOLVER_MIN_PROGRAM_SIZE;
		int maxInst=Constants.SOLVER_MAX_PROGRAM_SIZE;
		int nums=Constants.SOLVER_MAX_PROGRAM_SIZE+1;
		int bots = solvers.size;
		
		Array<int[]> combinations=new Array<int[]>();
		int[] comb;
		int sum;
		int j;
		boolean valid;
		
		for (int i=0; i<Math.pow(nums, bots); i++) {
			comb = new int[bots];
			j=i;
			for (int k=0; k<bots; k++) {
				comb[k]=j%nums;
				j=(j-comb[k])/nums;
			}
			sum=sum(comb);
			if (sum>=minInst && sum<=maxInst) {
				valid=true;
				for (int solverIndex=0; solverIndex<solvers.size; solverIndex++) {
					if (solvers.get(solverIndex).getProgram().getCells().size<comb[solverIndex] ||
							comb[solverIndex]<Constants.SOLVER_MIN_BOT_PROGRAM_SIZE) {
						valid=false;
						break;
					}
				}
				if (valid) {
					combinations.add(comb);
				}
			}
		}
		
		Sort.instance().sort(combinations, new Comparator<int[]>() {
			@Override
			public int compare(int[] c1, int[] c2) {
				return sum(c1)-sum(c2);
			}
		});
		return combinations;
	}
}
