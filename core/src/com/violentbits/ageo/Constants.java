package com.violentbits.ageo;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.graphics.Color;

public class Constants {
	/**
	 * The log level of the application.
	 */
	public static final int LOG_LEVEL = Application.LOG_DEBUG;
	/**
	 * World's width. Used 
	 */
	public static final float WORLD_WIDTH = 20.0f;
	/**
	 * World's height.
	 */
	public static final float WORLD_HEIGHT = 15.0f;
	/**
	 * The margin between the window and hud.
	 */
	public static final float HUD_MARGIN = 10.0f;
	/**
	 * Action duration when playing the game at slow speed.
	 */
	public static final float ACTION_TIME_SLOW = 0.8f;
	/**
	 * Action duration when playing the game at fast speed.
	 */
	public static final float ACTION_TIME_FAST = 0.2f;
	/**
	 * Tile size in pixels, in tmx maps.
	 */
	public static final float TILE_SIZE = 32f;
	/**
	 * Instruction icon size in level screen viewport coordinates.
	 */
	public static final float INSTRUCTION_ICON_SIZE = 64f;
	/**
	 * Viewport width used in all HUDs.
	 */
	public static final float LEVEL_SCREEN_WIDTH = 640f;
	/**
	 * Viewport height used in all HUDs.
	 */
	public static final float LEVEL_SCREEN_HEIGHT = 480f;
	/**
	 * Number of profiles that can be created in a single instance of the game.
	 */
	public static final int GAME_SLOTS = 3;
	/**
	 * Dark blue color used in the background.
	 */
	public static final Color DARK_BLUE = new Color(0x010133ff);
	/**
	 * Light blue color used in tiles background (floor).
	 */
	public static final Color BACKGROUND_BLUE = new Color(0x0b8eacff);
	/**
	 * Pink color used in tiles, buttons and in the menu screen background.
	 */
	public static final Color PINK = new Color(0xff0080ff); // 255,0,128
	/**
	 * Purple color used in tiles, and in the level screen icons.
	 */
	public static final Color PURPLE = new Color(0x260099ff); // 38,0,153
	/**
	 * Light gray color used to tint buttons when pressed
	 */
	public static final Color LIGHT_GRAY = new Color(0.8f, 0.8f, 0.8f, 1f);
	/**
	 * Gray color used in disabled buttons
	 */
	public static final Color GRAY = new Color(0.5f, 0.5f, 0.5f, 1f);
	/**
	 * Usual button width, in HUD coordinates.
	 */
	public static final float BUTTON_WIDTH = 100f;
	/**
	 * Usual button height, in HUD coordinates.
	 */
	public static final float BUTTON_HEIGHT = 50f;
	/**
	 * Separation between instruction icons in the main screen. In HUD coordinates.
	 */
	public static final float INSTRUCTIONS_SEPARATION = 5f;
	/**
	 * Set to true to activate the solver. In normal game this must be false.
	 */
	public static final boolean IS_SOLVER_ACTIVE = false;
	/**
	 * Number of steps that programs will run in the solver before jumping to the next solution candidate.
	 */
	public static final int SOLVER_LIMIT = 500;
	/**
	 * Minimum program size, including all editable bots, that the solver will try.
	 */
	public static final int SOLVER_MIN_PROGRAM_SIZE = 1;
	/**
	 * Maximum program size, including all editable bots, that the solver will try.
	 */
	public static final int SOLVER_MAX_PROGRAM_SIZE = 12;
	/**
	 * Minimum program size, for each editable bot, that the solver will try.
	 */
	public static final int SOLVER_MIN_BOT_PROGRAM_SIZE = 2;
	/**
	 * Set this to true to unblock all levels. In normal game this should be false.
	 */
	public static final boolean UNBLOCK_ALL = false;
}
