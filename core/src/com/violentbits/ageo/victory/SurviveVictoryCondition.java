package com.violentbits.ageo.victory;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.violentbits.ageo.apparat.ProgramListener;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.game.WorldListener;
import com.violentbits.ageo.instructions.Instruction;

public class SurviveVictoryCondition extends AbstractVictoryCondition implements WorldListener, ProgramListener {
	private int currentTime;
	private int time;
	private Bot bot;
	
	public SurviveVictoryCondition(WorldController worldController, int botId, int time) {
		this.time = time;
		for (Bot b : worldController.getBots()) {
			if (b.id == botId)
				bot = b;
		}
		worldController.addListener(this);
		bot.getProgram().addListener(this);
	}

	@Override
	public boolean checkVictory(WorldController worldController) {
		return currentTime >= time && bot.isActive();
	}

	@Override
	public void help(WorldController worldController, Stage worldStage, Stage hudStage) {
		String text = "Survive "+time+" instructions";
		createArrow(text, bot.getX(), bot.getY(), worldStage, hudStage);
	}

	@Override
	public void worldChanged(EventType event, Object object) {
		if (event == EventType.RESET)
			currentTime = 0;
	}

	@Override
	public void executed(Bot bot, Instruction instruction, int counter) {
		currentTime++;
	}

}
