package com.violentbits.ageo.victory;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class GoToVictoryCondition extends AbstractVictoryCondition {
	public final int botId;
	public final int x;
	public final int y;
	
	public GoToVictoryCondition(int botId, int x, int y) {
		this.botId = botId;
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean checkVictory(WorldController worldController) {
		boolean victory = false;
		Array<Bot> bots = worldController.getBots();
		for (Bot bot : bots) {
			if (bot.id == botId &&
					MathUtils.isEqual(bot.getX(), x, 0.4f) &&
					MathUtils.isEqual(bot.getY(), y, 0.4f)) {
				victory = true;
			}
		}
		return victory;
	}
	
	@Override
	public void help(WorldController worldController, final Stage worldStage, final Stage hudStage) {
		createArrow("Reach this point", x, y, worldStage, hudStage);
		Array<Bot> bots = worldController.getBots();
		for (Bot bot : bots) {
			if (bot.id == botId) {
				final Bot b = bot;
				hudStage.addAction(Actions.delay(arrowTime, Actions.run(new Runnable() {
					@Override
					public void run() {
						createArrow("using this", b.getX(), b.getY(), worldStage, hudStage);
					}
				})));
			}
		}
	}
}
