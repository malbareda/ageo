package com.violentbits.ageo.victory;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class KillVictoryCondition extends AbstractVictoryCondition {
	private int botId;
	private int numberToKill;
	
	public KillVictoryCondition(int botId, int numberToKill) {
		this.botId = botId;
		this.numberToKill = numberToKill;
	}

	@Override
	public boolean checkVictory(WorldController worldController) {
		int nKilled=0;
		
		for (Bot bot : worldController.getBots()) {
			if (bot.id==botId && !bot.isActive()) {
				nKilled++;
			}
		}
		return nKilled>=numberToKill;
	}

	@Override
	public void help(WorldController worldController, final Stage worldStage, final Stage hudStage) {
		String text;
		int totalNumber=0;
		Array<Bot> bots = worldController.getBots();
		Bot bid=null;
		for (Bot bot : bots) {
			if (bot.id == botId) {
				totalNumber++;
				bid=bot;
			}
		}
		if (numberToKill==1) {
			text = "Deactivate this";
			createArrow(text, bid.getX(), bid.getY(), worldStage, hudStage);
		} else if (totalNumber==numberToKill) {
			text = "Deactivate all of these";
			for (Bot bot : bots) {
				if (bot.id == botId) {
					createArrow(text, bot.getX(), bot.getY(), worldStage, hudStage);
					text="";
				}
			}
		} else {
			text = "Deactivate "+numberToKill+" of these";
			for (Bot bot : bots) {
				if (bot.id == botId) {
					createArrow(text, bot.getX(), bot.getY(), worldStage, hudStage);
					text="";
				}
			}
		}
		for (Bot bot : bots) {
			if (bot.editable) {
				final Bot b = bot;
				hudStage.addAction(Actions.delay(arrowTime, Actions.run(new Runnable() {
					@Override
					public void run() {
						createArrow("using this", b.getX(), b.getY(), worldStage, hudStage);
					}
				})));
			}
		}
	}

}
