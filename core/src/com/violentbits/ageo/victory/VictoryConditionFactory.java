package com.violentbits.ageo.victory;

import com.badlogic.gdx.Gdx;
import com.violentbits.ageo.game.MapController;
import com.violentbits.ageo.game.WorldController;

public class VictoryConditionFactory {
	private static final String TAG = VictoryConditionFactory.class.getName();

	public static VictoryCondition getVictoryCondition(WorldController worldController) {
		MapController mapController = worldController.getMapController();
		VictoryCondition victoryCondition = null;
		String goal = mapController.getMapProperty("goal");
		String[] goalParts = goal.split(" ");
		try {
			if (goalParts.length>0) {
				if (goalParts[0].equals("goto") && goalParts.length==4) {
					victoryCondition = new GoToVictoryCondition(
							Integer.parseInt(goalParts[1]),
							Integer.parseInt(goalParts[2]),
							Integer.parseInt(goalParts[3]));
				} else if (goalParts[0].equals("kill") && goalParts.length==3) {
					victoryCondition = new KillVictoryCondition(
							Integer.parseInt(goalParts[1]),
							Integer.parseInt(goalParts[2])
							);
				} else if (goalParts[0].equals("gotos")) {
					int i=1;
					ManyGoToVictoryCondition condition = new ManyGoToVictoryCondition();
					while (i<goalParts.length) {
						condition.addVictoryCondition(
								Integer.parseInt(goalParts[i]),
								Integer.parseInt(goalParts[i+1]),
								Integer.parseInt(goalParts[i+2])
						);
						i+=3;
					}
					victoryCondition = condition;
				} else if (goalParts[0].equals("survive")) {
					victoryCondition = new SurviveVictoryCondition(worldController,
							Integer.parseInt(goalParts[1]),
							Integer.parseInt(goalParts[2]));
				}
			}
		} catch (NumberFormatException e) {
			Gdx.app.error(TAG, "Can't create victory condition: "+e.getMessage());
		} catch (ArrayIndexOutOfBoundsException e) {
			Gdx.app.error(TAG, "Can't create victory condition: "+e.getMessage());
		}
		return victoryCondition;
	}
}
