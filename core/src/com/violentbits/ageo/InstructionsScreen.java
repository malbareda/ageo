package com.violentbits.ageo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.violentbits.ageo.io.Assets;

/**
 * This simple screen shows some basic game instructions to the user.
 */
public class InstructionsScreen extends AbstractGameScreen {
	private Stage stage;
	private Skin skin;
	private Table table;
	private VerticalGroup col3;
	private Label label1a, label1b, label1c, label2a, label3, label4a, label4b, label5a, label5b,
			label5c, label6a, label6b;
	private Image image1, image2a, image2b, image2c, image2d, image2e, image2f, image2g, image2h,
			image3a, image3b, image3c, image3d, image5, image6;
	private HorizontalGroup row1, row1a, row2, row3, row5, row5a, row6;
	private TextButton button;
	
	private InputAdapter inputAdapter = new InputAdapter() {
		@Override
		public boolean keyUp(int keycode) {
			if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {
				game.setScreen(new MenuScreen((AgeoGame)game));
			}
			return false;
		}
	};

	public InstructionsScreen(AgeoGame game) {
		super(game);
		skin = Assets.instance.skin;
	}
	
	private void buildRow1() {
		label1a = new Label("1. Tap on a bot ", skin);
		image1 = new Image(Assets.instance.get("round_bot"));
		image1.setColor(Color.GREEN);
		label1b = new Label(" to see or edit its program. ", skin);
		label1c = new Label("Not all bots can be edited.", skin);
		label1c.setColor(Color.GRAY);
		label1c.setFontScale(0.85f);
		row1 = new HorizontalGroup();
		row1a = new HorizontalGroup();
		row1a.addActor(label1a);
		row1a.addActor(image1);
		row1a.addActor(label1b);
		row1.addActor(row1a);
		row1.addActor(label1c);
		table.add(row1).left().row();
	}
	
	private void buildRow2() {
		label2a = new Label("2. The current bot program is displayed at the bottom of the screen:", skin);
		image2a = new Image(Assets.instance.get("Left"));
		image2b = new Image(Assets.instance.get("Left"));
		image2c = new Image(Assets.instance.get("Up"));
		image2d = new Image(Assets.instance.get("Right"));
		image2e = new Image(Assets.instance.get("Up"));
		image2f = new Image(Assets.instance.get("Left"));
		image2g = new Image(Assets.instance.get("void"));
		image2h = new Image(Assets.instance.get("void"));
		row2 = new HorizontalGroup();
		row2.addActor(image2a);
		row2.addActor(image2b);
		row2.addActor(image2c);
		row2.addActor(image2d);
		row2.addActor(image2e);
		row2.addActor(image2f);
		row2.addActor(image2g);
		row2.addActor(image2h);
		table.add(label2a).left().row();
		table.add(row2).center().row();
	}
	
	private void buildRow3() {
		label3 = new Label("3. Tap the instructions on the right to add instructions to the program:", skin);
		image3a = new Image(Assets.instance.get("Up"));
		image3b = new Image(Assets.instance.get("Down"));
		image3c = new Image(Assets.instance.get("Right"));
		image3d = new Image(Assets.instance.get("Left"));
		col3 = new VerticalGroup();
		col3.addActor(image3a);
		col3.addActor(image3b);
		col3.addActor(image3c);
		col3.addActor(image3d);
		row3 = new HorizontalGroup();
		row3.space(10);
		row3.addActor(label3);
		row3.addActor(col3);
		table.add(row3).left().row();
	}
	
	private void buildRow4() {
		label4a = new Label("4. Double tap on a program's instruction to remove it.", skin);
		label4b = new Label("You can also rearrange instructions by dragging them.", skin);
		label4b.setColor(Color.GRAY);
		label4b.setFontScale(0.85f);
		table.add(label4a).left().row();
		table.add(label4b).padLeft(18).left().row();
	}
	
	private void buildRow5() {
		label5a = new Label("5. Use ", skin);
		image5 = new Image(Assets.instance.get("play"));
		label5b = new Label(" to run your program. ", skin);
		label5c = new Label("You can't change a program while it's running.", skin);
		label5c.setColor(Color.GRAY);
		label5c.setFontScale(0.85f);
		row5 = new HorizontalGroup();
		row5a = new HorizontalGroup();
		row5a.addActor(label5a);
		row5a.addActor(image5);
		row5a.addActor(label5b);
		row5.addActor(row5a);
		row5.addActor(label5c);
		table.add(row5).left().row();
	}
	
	private void buildRow6() {
		label6a = new Label("6. Use ", skin);
		image6 = new Image(Assets.instance.get("stop"));
		label6b = new Label(" to reset the level.", skin);
		row6 = new HorizontalGroup();
		row6.addActor(label6a);
		row6.addActor(image6);
		row6.addActor(label6b);
		table.add(row6).left().row();
	}
	
	private void buildRow7() {
		button = new TextButton("Back", skin);
		button.addListener(new ClickListener() {
			@Override
	    	public void clicked(InputEvent event, float x, float y) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				game.setScreen(new MenuScreen((AgeoGame)game));
			}
		});
		
		table.add(button).size(Constants.BUTTON_WIDTH,Constants.BUTTON_HEIGHT).center().row();
	}
	
	private void buildStage() {
		table = new Table();
		buildRow1();
		buildRow2();
		buildRow3();
		buildRow4();
		buildRow5();
		buildRow6();
		buildRow7();
		
		table.setFillParent(true);
		stage.addActor(table);
		
		setAlpha(0f);
		createActions();
		stage.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent e, float x, float y) {
				setAlpha(1f);
			}
		});
	}
	
	private void setAlpha(float a) {
		row1a.getColor().a=a;
		label1c.getColor().a=a;
		label2a.getColor().a=a;
		row2.getColor().a=a;
		label3.getColor().a=a;
		col3.getColor().a=a;
		label4a.getColor().a=a;
		label4b.getColor().a=a;
		row5a.getColor().a=a;
		label5c.getColor().a=a;
		row6.getColor().a=a;
		button.getColor().a=a;
	}
	
	private void createActions() {
		float actionTime = 2.4f;
		
		Action action1a = Actions.fadeIn(actionTime);
		action1a.setTarget(row1a);
		Action action1b = Actions.fadeIn(actionTime);
		action1b.setTarget(label1c);
		Action action2a = Actions.fadeIn(actionTime);
		action2a.setTarget(label2a);
		Action action2b = Actions.fadeIn(actionTime);
		action2b.setTarget(row2);
		Action action3a = Actions.fadeIn(actionTime);
		action3a.setTarget(label3);
		Action action3b = Actions.fadeIn(actionTime);
		action3b.setTarget(col3);
		Action action4a = Actions.fadeIn(actionTime);
		action4a.setTarget(label4a);
		Action action4b = Actions.fadeIn(actionTime);
		action4b.setTarget(label4b);
		Action action5a = Actions.fadeIn(actionTime);
		action5a.setTarget(row5a);
		Action action5b = Actions.fadeIn(actionTime);
		action5b.setTarget(label5c);
		Action action6 = Actions.fadeIn(actionTime);
		action6.setTarget(row6);
		Action action7 = Actions.fadeIn(actionTime);
		action7.setTarget(button);
		stage.addAction(Actions.sequence(
			action1a, action1b, action2a, action2b, action3a, action3b,
			action4a, action4b, action5a, action5b, action6, action7
		));
	}

	@Override
	public void show() {
		Gdx.input.setCatchBackKey(true);
		stage = new Stage(new ExtendViewport(Constants.LEVEL_SCREEN_WIDTH, Constants.LEVEL_SCREEN_HEIGHT));
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(inputAdapter);
        Gdx.input.setInputProcessor(inputMultiplexer);
		buildStage();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(Constants.DARK_BLUE.r, Constants.DARK_BLUE.g, Constants.DARK_BLUE.b, Constants.DARK_BLUE.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		stage.dispose();
	}
}
