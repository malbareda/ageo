package com.violentbits.ageo;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.removeActor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.violentbits.ageo.game.GameScreen;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.io.LevelInfo;
import com.violentbits.ageo.io.LevelStage;
import com.violentbits.ageo.io.Profile;
import com.violentbits.ageo.scene.MiniStatsTable;

/**
 * This screen allows the selection of a level.
 */
public class LevelScreen extends AbstractGameScreen {
	private static final String TAG = LevelScreen.class.getName();
	private Stage stage;
	private LevelStage levelStage;
	private Skin skin;
	private int levelStageIndex;
	private float accDelta = 0f;
	private Actor robot;
	private TextButton btnStart;
	private TextButton prevStageBtn;
	private TextButton nextStageBtn;

	private Profile profile;

	private boolean win;
	private Array<Stack> places;

	private InputAdapter inputAdapter = new InputAdapter() {
		@Override
		public boolean keyUp(int keycode) {
			// Back to level selection screen
			if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {
				game.setScreen(new MenuScreen((AgeoGame) game));
			}
			return false;
		}
	};

	private void moveRobot(int posDest) {
		SequenceAction action = Actions.sequence();
		int posOrigin = (Integer) robot.getUserObject();

		Gdx.app.debug(TAG, posOrigin + " -> " + posDest);
		if (posDest > posOrigin) {
			for (int pos = posOrigin + 1; pos <= posDest; pos++) {
				action.addAction(Actions.moveTo(levelStage.levels.get(pos).pos[0] - robot.getWidth(),
						levelStage.levels.get(pos).pos[1] + robot.getHeight(), 0.5f));
			}
		} else if (posDest < posOrigin) {
			for (int pos = posOrigin - 1; pos >= posDest; pos--) {
				action.addAction(Actions.moveTo(levelStage.levels.get(pos).pos[0] - robot.getWidth(),
						levelStage.levels.get(pos).pos[1] + robot.getHeight(), 0.5f));
			}
		}
		GamePreferences.instance.getCurrentProfile().setLevelPos(posDest);
		robot.setUserObject(new Integer(posDest));
		robot.addAction(action);

		for (int i = 0; i < places.size; i++) {
			Actor processor = places.get(i).getChildren().first();
			processor.clearActions();
			processor.setColor(getProcessorColor(i));
		}
		Actor dest = places.get(posDest).getChildren().first();
		Color color = dest.getColor().cpy();
		color.mul(2f);
		dest.addAction(Actions.color(color, Constants.ACTION_TIME_SLOW));
	}

	private ClickListener placeListener = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			if (!robot.hasActions()) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				int posDest = (Integer) event.getListenerActor().getUserObject();
				moveRobot(posDest);
			}
		}
	};

	@SuppressWarnings("unused")
	private boolean isLocked(int levelPos) {
		boolean locked = true;
		LevelInfo lastLevel;

		if (levelPos == 0) {
			if (levelStageIndex == 0) {
				locked = false;
			} else {
				LevelStage prevStage = Assets.instance.getStages().get(levelStageIndex - 1);
				lastLevel = prevStage.levels.peek();
				locked = !profile.isWon(lastLevel.name);
			}
		} else {
			lastLevel = levelStage.levels.get(levelPos - 1);
			locked = !profile.isWon(lastLevel.name);
		}
		if (Constants.IS_SOLVER_ACTIVE || Constants.UNBLOCK_ALL)
			locked = false;
		return locked;
	}

	/**
	 * 
	 * @param game
	 * @param win
	 *            If true, an animation is shown on the last visited level.
	 */
	public LevelScreen(AgeoGame game, boolean win) {
		super(game);
		skin = Assets.instance.skin;
		init();
		this.win = win;
	}

	public LevelScreen(AgeoGame game) {
		this(game, false);
	}

	private void init() {
		profile = GamePreferences.instance.getCurrentProfile();
		levelStageIndex = profile.getCurrentStage();
		levelStage = Assets.instance.getStages().get(levelStageIndex);
		places = new Array<Stack>();
	}

	@Override
	public void show() {
		Gdx.input.setCatchBackKey(true);
		Gdx.graphics.setContinuousRendering(false);
		Gdx.graphics.requestRendering();
		stage = new Stage(new ExtendViewport(Constants.LEVEL_SCREEN_WIDTH, Constants.LEVEL_SCREEN_HEIGHT));
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(stage);
		inputMultiplexer.addProcessor(inputAdapter);
		Gdx.input.setInputProcessor(inputMultiplexer);
		buildStage();
		if (win)
			winAnimation();
	}

	private void winAnimation() {
		win = false;
		int currentPos = (Integer) robot.getUserObject();
		if (currentPos < levelStage.levels.size - 1) {
			// Move to next level
			Actor processor = places.get(currentPos).getChildren().first();
			processor.setColor(Constants.PURPLE);
			processor
					.addAction(Actions.sequence(
							Actions.repeat(5,
									Actions.sequence(Actions.color(Constants.PINK, 0.1f),
											Actions.color(Constants.PURPLE, 0.1f))),
							Actions.color(Constants.PINK, 2f)));
			moveRobot(currentPos + 1);
		} else {
			// Move to next stage
			robot.addAction(
					Actions.sequence(Actions.moveTo(stage.getWidth(), robot.getY(), 2f), Actions.run(new Runnable() {
						@Override
						public void run() {
							nextStage();
						}
					})));
		}
	}

	private void buildLines() {
		for (int i = 1; i < levelStage.levels.size; i++) {
			LevelInfo level = levelStage.levels.get(i);
			LevelInfo levelAnt = levelStage.levels.get(i - 1);
			Image line = new Image(Assets.instance.getNinePatch("line"));
			float dist = (float) Math.sqrt((level.pos[0] - levelAnt.pos[0]) * (level.pos[0] - levelAnt.pos[0])
					+ (level.pos[1] - levelAnt.pos[1]) * (level.pos[1] - levelAnt.pos[1]));
			line.setWidth(dist + 12f);
			line.setHeight(12f);
			line.setColor(Constants.BACKGROUND_BLUE);
			line.setTouchable(Touchable.disabled);
			Container<Image> container = new Container<Image>(line);
			container.fill();
			container.setTransform(true);
			container.setSize(line.getWidth(), line.getHeight());
			container.setOrigin(6f, 6f);
			container.setPosition(levelAnt.pos[0] + 32f - 6f, levelAnt.pos[1] + 32f - 6f);
			container.setRotation(
					MathUtils.radDeg * MathUtils.atan2(level.pos[1] - levelAnt.pos[1], level.pos[0] - levelAnt.pos[0]));
			container.setTouchable(Touchable.disabled);
			stage.addActor(container);
		}
	}

	private Actor buildRobot() {
		int initialPos = profile.getLevelPos();
		Actor robot = new Image(Assets.instance.get("robot"));
		robot.setColor(Constants.PINK);
		robot.setSize(32f, 32f);
		robot.setPosition(levelStage.levels.get(initialPos).pos[0] - robot.getWidth(),
				levelStage.levels.get(initialPos).pos[1] + robot.getHeight());
		robot.setUserObject(initialPos);
		robot.setTouchable(Touchable.disabled);
		return robot;
	}

	private TextButton buildStartButton() {
		TextButton btnStart = new TextButton("Start", skin);
		btnStart.setSize(Constants.BUTTON_WIDTH, Constants.BUTTON_HEIGHT);
		btnStart.setPosition((stage.getWidth() - btnStart.getWidth()) * 0.5f, Constants.HUD_MARGIN);
		btnStart.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				int levelPos = (Integer) robot.getUserObject();
				if (isLocked(levelPos)) {
					SoundController.instance.play(Assets.instance.getSound("no.ogg"));
				} else {
					SoundController.instance.play(Assets.instance.getSound("select.wav"));
					LevelInfo levelInfo = levelStage.levels.get((Integer) robot.getUserObject());
					;
					Gdx.app.debug(TAG, "Level " + levelInfo.name + " selected.");
					if (levelInfo.messages != null) {
						game.setScreen(new IntroScreen((AgeoGame) game, levelInfo));
					} else {
						Assets.instance.loadLevel(levelInfo);
						game.setScreen(new GameScreen((AgeoGame) game));
					}
				}
			}
		});
		return btnStart;
	}

	private Label buildTitleLabel() {
		Label titleLabel = new Label(levelStage.name, skin, "title");
		titleLabel.setPosition((stage.getWidth() - titleLabel.getWidth()) * 0.5f,
				stage.getHeight() - titleLabel.getHeight());
		titleLabel.setColor(1, 1, 1, 0);
		titleLabel.setTouchable(Touchable.disabled);
		titleLabel.addAction(sequence(fadeIn(2), delay(3), fadeOut(2), removeActor()));
		return titleLabel;
	}

	private Color getProcessorColor(int levelPos) {
		Color color;
		LevelInfo level = levelStage.levels.get(levelPos);
		if (profile.isWon(level.name)) {
			color = Constants.PINK;
		} else {
			color = Constants.PURPLE;
		}
		return color;
	}

	private Stack buildPlace(int levelPos) {
		Stack place = new Stack();
		LevelInfo level = levelStage.levels.get(levelPos);
		Actor processor = new Image(Assets.instance.get("processor"));
		place.setSize(processor.getWidth(), processor.getHeight());
		place.setPosition(level.pos[0], level.pos[1]);
		processor.setColor(getProcessorColor(levelPos));
		if (profile.isWon(level.name)) {
			MiniStatsTable stats = new MiniStatsTable(level.name);
			stats.setPosition(level.pos[0] + (place.getWidth() - stats.getWidth()) / 2f,
					level.pos[1] - stats.getHeight());
			stage.addActor(stats);
		}
		place.setUserObject(new Integer(levelPos));
		place.addListener(placeListener);
		place.addActor(processor);
		if (isLocked(levelPos)) {
			Actor padlock = new Image(Assets.instance.get("padlock"));
			place.addActor(padlock);
		}
		if (levelPos == profile.getLevelPos()) {
			processor.setColor(processor.getColor().mul(2f));
		}
		return place;
	}

	private void nextStage() {
		if (levelStageIndex < Assets.instance.getStages().size - 1) {
			SoundController.instance.play(Assets.instance.getSound("select.wav"));
			levelStageIndex++;
			profile.setCurrentStage(levelStageIndex);
			init();
			game.setScreen(LevelScreen.this);
		}
	}

	private void prevStage() {
		if (levelStageIndex > 0) {
			SoundController.instance.play(Assets.instance.getSound("select.wav"));
			levelStageIndex--;
			profile.setCurrentStage(levelStageIndex);
			init();
			game.setScreen(LevelScreen.this);
		}
	}

	private TextButton buildNextStageButton() {
		TextButton nextStageBtn = new TextButton(">", skin);
		nextStageBtn.setSize(50, 150);
		nextStageBtn.setPosition(stage.getWidth() - nextStageBtn.getWidth(),
				(stage.getHeight() - nextStageBtn.getHeight()) / 2f);
		nextStageBtn.setVisible(levelStageIndex < Assets.instance.getStages().size - 1);
		nextStageBtn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				nextStage();
			}
		});
		return nextStageBtn;
	}

	private TextButton buildPrevStageButton() {
		TextButton prevStageBtn = new TextButton("<", skin);
		prevStageBtn.setSize(50, 150);
		prevStageBtn.setPosition(0, (stage.getHeight() - prevStageBtn.getHeight()) / 2f);
		prevStageBtn.setVisible(levelStageIndex > 0);
		prevStageBtn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				prevStage();
			}
		});
		return prevStageBtn;
	}

	private void buildStage() {
		buildLines();
		for (int i = 0; i < levelStage.levels.size; i++) {
			Stack place = buildPlace(i);
			places.add(place);
			stage.addActor(place);
		}
		robot = buildRobot();
		stage.addActor(robot);
		btnStart = buildStartButton();
		stage.addActor(btnStart);
		Label titleLabel = buildTitleLabel();
		stage.addActor(titleLabel);
		nextStageBtn = buildNextStageButton();
		prevStageBtn = buildPrevStageButton();
		stage.addActor(nextStageBtn);
		stage.addActor(prevStageBtn);
	}

	private void addSphere(int i, int j) {
		Actor sphere = new Image(Assets.instance.get("sphere"));
		sphere.setColor(Constants.PINK);
		int n = MathUtils.random(1, 8);
		sphere.setScale(0.5f + 0.1f * n);
		sphere.setPosition(levelStage.levels.get(i).pos[0] + 32f - sphere.getWidth() * sphere.getScaleX() * 0.5f,
				levelStage.levels.get(i).pos[1] + 32f - sphere.getHeight() * sphere.getScaleY() * 0.5f);
		sphere.addAction(Actions.parallel(
				Actions.moveTo(levelStage.levels.get(j).pos[0] + 32f - sphere.getWidth() * sphere.getScaleX() * 0.5f,
						levelStage.levels.get(j).pos[1] + 32f - sphere.getHeight() * sphere.getScaleY() * 0.5f,
						1.5f + 0.1f * n),
				Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(0.5f + 0.1f * n), Actions.fadeOut(0.5f),
						Actions.removeActor())));
		sphere.setTouchable(Touchable.disabled);
		stage.addActor(sphere);
		SoundController.instance.play(Assets.instance.getSound("sphere" + n + ".ogg"));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(Constants.DARK_BLUE.r, Constants.DARK_BLUE.g, Constants.DARK_BLUE.b, Constants.DARK_BLUE.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		accDelta += delta;
		if (accDelta >= 1f && MathUtils.random() < 0.1f && levelStage.levels.size > 1) {
			int n;
			if (MathUtils.randomBoolean()) {
				n = MathUtils.random(levelStage.levels.size - 2);
				addSphere(n, n + 1);
			} else {
				n = MathUtils.random(1, levelStage.levels.size - 1);
				addSphere(n, n - 1);
			}
			accDelta = 0;
		}
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
		btnStart.setPosition((stage.getWidth() - btnStart.getWidth()) * 0.5f, Constants.HUD_MARGIN);
		prevStageBtn.setPosition(0, (stage.getHeight() - prevStageBtn.getHeight()) / 2f);
		nextStageBtn.setPosition(stage.getWidth() - nextStageBtn.getWidth(),
				(stage.getHeight() - nextStageBtn.getHeight()) / 2f);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		stage.dispose();
	}
}
