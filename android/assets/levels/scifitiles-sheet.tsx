<?xml version="1.0" encoding="UTF-8"?>
<tileset name="scifitiles-sheet" tilewidth="32" tileheight="32" tilecount="84" columns="14">
 <image source="scifitiles-sheet.png" width="448" height="192"/>
 <tile id="40">
  <properties>
   <property name="block" value="up"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="41">
  <properties>
   <property name="block" value="right"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="46">
  <properties>
   <property name="block" value="up down right left"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="block" value="up left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="48">
  <properties>
   <property name="block" value="up"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="49">
  <properties>
   <property name="block" value="up right"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="50">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="51">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="52">
  <properties>
   <property name="block" value="up left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="53">
  <properties>
   <property name="block" value="up right"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="54">
  <properties>
   <property name="block" value="left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="55">
  <properties>
   <property name="block" value="down"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="56">
  <properties>
   <property name="block" value="up right left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="57">
  <properties>
   <property name="block" value="down right left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="60">
  <properties>
   <property name="block" value="right left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="61">
  <properties>
   <property name="block" value="left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="62">
  <properties>
   <property name="trigger" value="SelfDestruct"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="63">
  <properties>
   <property name="block" value="right"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="64">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="65">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="66">
  <properties>
   <property name="block" value="down left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="67">
  <properties>
   <property name="block" value="down right"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="68">
  <properties>
   <property name="block" value="right down left"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="block" value="up right left"/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="block" value="up down left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="71">
  <properties>
   <property name="block" value="up down right"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="74">
  <properties>
   <property name="block" value="up down"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="75">
  <properties>
   <property name="block" value="down left"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="76">
  <properties>
   <property name="block" value="down"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="77">
  <properties>
   <property name="block" value="down right"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="78">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="79">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="80">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="81">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="82">
  <properties>
   <property name="block" value="up down left"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="block" value="up right down"/>
  </properties>
 </tile>
</tileset>
